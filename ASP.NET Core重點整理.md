[TOC]

# ASP.NETC Core重點整理

## 一、環境篇

### 版本說明

1. 目前穩定建議版本是3.1.5,下一個世代版本是5.x版,目前己有preview版試用
   1. dotnet core的下一代的名稱是叫vNext,.NET 5就是vNext
   2. .NET 5版是原來.net 4.8及core一起合併升級,成為統一版本(a unified platform)
   3. 同時也為了避免和原4.8版的混淆,所以才一次跳到5版
   4. 可以參考這裡的介紹: https://devblogs.microsoft.com/dotnet/introducing-net-5/
2. dotnet有二種發佈型態: 
   - runtime:僅提供執行環境,此版本又分為以下幾種小分支
     - dotnet core runtime: 提供console模式下的執行環境
     - Desktop core runtime: 僅支援Windows平台,提供Windows application的執行環境(安裝此版會同時包含dotnet core runtime)
     - ASP.NET core runtime: 用來支援ASP.NET相關的程式(即Web端的執行環境,提供非Windows平台上的執行)
     - ASP.NET Hosting Bundle: 同上,並支援IIS環境,如果是要在Windows/Windows Server提供執行環境,建議安裝此版即可
   - SDK: 系統開發版本,包括所有runtime內容,及開發所需要的編譯工具

### 各作業系統的支援程度

以下支援的Windows版本,僅以dotnet core 3.1版為準

| OS                | 版本         | 架構       |
| :---------------- | :----------- | :--------- |
| Windows 用戶端    | 7 SP1 +、8.1 | x64、x86   |
| Windows 10 用戶端 | 版本 1607 +  | x64、x86   |
| Windows Server    | 2012 R2 +    | x64、x86   |
| Nano 伺服器       | 版本 1803 +  | x64、ARM32 |

以下作業系統下版本：

- Windows 7 SP1
- Windows Vista SP 2
- Windows 8.1
- Windows Server 2008 R2
- Windows Server 2012 R2

要先安裝下列項目：

1. [Microsoft Visual C++ 2015](https://www.microsoft.com/download/details.aspx?id=52685)可轉散發套件 Update 3。
2. [KB2533623](https://support.microsoft.com/help/2533623/microsoft-security-advisory-insecure-library-loading-could-allow-remot)

### 安裝

微軟提供各種平台及安裝方式,可以由: https://dotnet.microsoft.com/download/dotnet-core/3.1 進入選擇適合自己的平台及版本安裝,如果是安裝Visual Studio 2019(Windows版),可以勾選.net core的選項(在最右下角,預設未勾),亦會安裝,但由於Visual Studio的更新頻率不是很高,所以建議是統一由前述網址來安裝!各平台建議安裝的開發環境如下:

1. Windows: https://dotnet.microsoft.com/download/dotnet-core/thank-you/sdk-3.1.105-windows-x64-installer

2. macOS: https://dotnet.microsoft.com/download/dotnet-core/thank-you/sdk-3.1.105-macos-x64-installer

   註: macOS Catalina安裝注意事項: https://docs.microsoft.com/zh-tw/dotnet/core/install/macos-notarization-issues

3. Ubuntu版本,可以package manager來安裝,以下是18.04版的安裝範例:

   安裝簽署金鑰(僅需安裝一次):

   ```bash
   wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
   sudo dpkg -i packages-microsoft-prod.deb
   ```

   安裝dotnet core sdk

   ```bash
   sudo apt-get update; \
     sudo apt-get install -y apt-transport-https && \
     sudo apt-get update && \
     sudo apt-get install -y dotnet-sdk-3.1
   ```

   其它ubuntu的版本安裝說明:https://docs.microsoft.com/zh-tw/dotnet/core/install/linux-ubuntu

   其它linux的版本安裝方法:https://docs.microsoft.com/zh-tw/dotnet/core/install/linux


### 檢查版本

1. 列出己安裝的sdk: `dotnet --list-sdks`
2. 列出己安裝的runtime: `dotnet --list-runtimes`

### 開發工具

1. Visual Studio 2019版(含)以上,目前還是以這個功能最完整,但因為本身過於龐大,平時開發可以有其它選擇.建議要安裝,已備不時之需!
2. Visual Studio Code(簡稱VSCode,免費): 這是最推薦日常開發使用的工具,功能大多以擴充檔來補強,因此在整合度上略有不足,很多操作要以輸入命令的方式達成,但是一但上手之後,效率會很高!
3. Jetbrain Rider: 這個IDE極好用,功能也很完整(但還是沒有Visual Studio完整),在非Windows的環境之下,可以考慮使用,但這套是付費軟體!
4. 由於dotnet core的使用,可以用很簡單的命令模式來做編譯及執行,因此在其它任何文字編輯器下亦都能使用,只是方便程度上的差異而己!

### 其它說明

1. C#的版本最好用8.0或以上的版本,很多新的功能是依賴新的語法來達成而跟dotnet core架構本身無關!
2. VSCode推薦安裝的擴充模組: https://marketplace.visualstudio.com/items?itemName=doggy8088.netcore-extension-pack (這是一個整合包,不是單一包,就是保哥把所有相關的套件都集合在一起,不用一個一個裝)
3. 要特別注意,到目前為止,Windows form及WPF的程式(Desktop),還不能在其它平台上開發及執行!(.Net 5.0都尚不支援)
4. dotnet core的專案架構跟dotnet framework的專案架構差異很大,基本上是不相容!若想要將原本dotnet framework專案移植到dotnet core,微軟提供一套工具可以試試: https://docs.microsoft.com/zh-tw/dotnet/core/porting/
5. Asp.net core在各平台上,會預設自帶一個網站伺服器(這),這伺服器名為Kestrel,

### dotnet CLI (Command Line Interface)

為了要能方便的在各種平台使用及開發,dotnet提供了一個`dotnet`命令,來做各種操作(可以打入`dotnet help`來查看說明文件

#### 1. 建立專案

建立專案的方式是以內建的樣版`templates`來協助製作一個專案,而當前的樣版可以透過`dotnet new`來查看:(下表未列出全部)

```
Templates                        Short Name     Language     Tags
-----------------------------------------------------------------------------------
Console Application              console        [C#], F#, VB Common/Console
Class library                    classlib       [C#], F#, VB Common/Library
WPF Application                  wpf            [C#]         Common/WPF
Windows Forms (WinForms) Applic. winforms       [C#]         Common/WinForms
Windows Forms (WinForms) Class.. winformslib    [C#]         Common/WinForms
Worker Service                   worker         [C#]         Common/Worker/Web
Razor Component                  razorcomponent [C#]         Web/ASP.NET
Razor Page                       page           [C#]         Web/ASP.NET
MVC ViewImports                  viewimports    [C#]         Web/ASP.NET
MVC ViewStart                    viewstart      [C#]         Web/ASP.NET
Blazor Server App                blazorserver   [C#]         Web/Blazor
Blazor WebAssembly App           blazorwasm     [C#]         Web/Blazor/WebAssembly
ASP.NET Core Empty               web            [C#], F#     Web/Empty
```

而其中`short name`即為要指定建立專案的名稱,例如:

- `dotnet new console`: 會在當前目錄下,建立一個console(指定template的short name)的專案
- `dotnet new web --name demoweb`: 會建立`demoweb`目錄,並在此目錄下建立一個web的專案,專案名稱亦為`demoweb`(專案檔為`demoweb.csproj`),如果需要方案檔,需要另外下指令建立

註: templates的項目是可以另外增加/移除的!

#### 2.建立方案(solution)

建立好專案後,可以依下列指令來建立方案檔(.sln)

```bash
dotnet new sln // 這會建立一個同目錄名稱的方案檔,如果當前目錄下不是專案(專案在子目錄下),則產生的方案是沒有包括專案的
dotnet sln add `PROJECT_NAME`  // 加入一個專案,PROJECT_NAME是指定的專案(子目錄名稱)
```
此外
`dotnet sln list`: 可以列出當前目錄下的sln檔案內的專案清單
`dotnet sln remove PROJECT_NAME`:  移除一個`PROJECT_NAME`專案

#### 3.建置(編譯)專案

建置一個專案/方案,常用的指令如下(可透過`dotnet build --help`查看較完整的說明)

- `dotnet build`:可以對當前目錄下的專案做建置(build),並在建置前會自動下載安裝nuget套件(nuget package),在預設情況下,建置過程會產生`obj`及`bin`目錄,前者是做編譯後產生的中間檔案,後者是放編譯後的`dll`,執行檔案及相關所需要的產出檔案
- `dotnet build -o output`: 可以將最終產出由原來`bin`目錄改為`output`目錄
- `dotnet build testweb.csproj`: 建置指定的專案(testweb.csproj專案),亦可指定一個方案檔案,來對整個方案做建置

#### 4.運行專案

- `dotnet run`: 執行當前目錄下的專案
- `dotnet run --project /path/to/project` : 執行當前目錄下的專案

#### 5.清除建置專案(非刪除專案)

- `dotnet clean`將建置的專案結果(`bin`目錄)清除

#### 6.nuget套件管理

套件查詢,可以透過: https://www.nuget.org/ 此網站來查詢相關套件,如果想要用CLI來查詢套件,官方本身並不支援此功能,必需安裝CLI的外掛,可以參考[`9.外掛工具`](# 9.外掛工具)的說明,此外`dotnet nuget`一系列的指令,均與本主題無關,這是用來自製/發佈/管理nuget套件的功能,請勿混淆!

- `dotnet add package PACKAGE_NAME`: 加入一個最新版本的`PACKAGE_NAME`的nuget套件

- `dotnet add package PACKAGE_NAME --version x.y.z`: 加入一個最新版本的PACKAGE_NAME的nuget套件,並指定版本`x.y.z`

- `dotnet add package PACKAGE_NAME:x.y.z`: 同上

- `dotnet list package`:列出當前專案目錄下,己安裝的套件

- `dotnet list PROJECT_NAME package`:列出`PROJECT_NAME`專案目錄下,己安裝的套件(PROJECT_NAME也可以是一個方案名稱檔名)

- `dotnet remove package PACKAGE_NAME`:移除當前目錄專案下的`PACKAGE_NAME`套件

- `dotnet remove PROJECT_NAME package PACKAGE_NAME`:移除當`PROJECT_NAME`專案下的`PACKAGE_NAME`套件

- `dotnet restore`: 重新下載套件,下載套件動作基本上在加入套件/建置專案時,均會自動下載,若要手動強制下載套件,可用此指令

#### 7.參考(關聯)專案

在同一個方案中,若一個專案要加入(或相關操作)另一個專案做為參考專案(基本上做法跟nuget套件一樣):

- `dotnet add reference /path/to/PROJECT_NAME`: 加入一個`PROJECT_NAME`為參考專案

- `dotnet list reference`:列出當前專案,己參考的專案

- `dotnet remove reference /path/to/PROJECT_NAME`:移除當前目錄專案下的`PROJECT_NAME`參考專案

#### 8.佈署

若要佈署專案到指定位置,可以製作一個發佈包,此包(即目錄)會包括所有相關及需要的檔案,只要將此目錄下的所有檔案打包,放置到要佈署的地方即可

- `dotnet publish`: 製作發佈目錄,放置在`bin/Debug|Release/netcoreapp3.1/publish`目錄下

此外:
1. 由於此指令的相關選項很多,可以參考此網頁詳細說明: https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-publish
2. 若要直接佈署到遠端主機或IIS上,建議使用Visual Studio(首推,這是Visual Studio最有價值的地方之一)及Visual Studio Code(需要裝擴充套件才比較完整)

#### 9.外掛工具

dotnet CLI本身一般常用功能還算簡單好用,但是整體來說,還是缺了不少功能,因此它提供了一個方法,可以用來擴充其功能

- `dotnet tool install -g PACKAGE_ID`: 安裝一個指定名稱的外掛工具,`-g`是安裝後提供給所有人使用(global),例如:`dotnet tools install -g dotnet-search`會安裝用來查詢nuget套件庫的內容,安裝完後即可以以下方式來使用(實際用法要以各工具擴充的文件說明為主)
  - `dotnet search KEYWORD`: 例如:`dotnet search swagger`,會查出所有有`swagger`字眼的套件
- `dotnet tool install PACKAGE_ID` 會將其工具安裝在當前目錄下(在此目錄下才生效),同時在此目錄下,必須先建立一個設定檔,建立方式:`dotnet new tool-manifest`(一般來說,是安裝在某一個專案的根目錄下,而此工具是專給此專案使用),再安裝本地工具,例如:`dotnet tool install dotnet-depends`,即可以在此目錄下使用此工具:
  - `dotnet dotnet-depends`: 會列出此專案所有相依套件及專案(此工具其實還是安裝成global比較好用,這只是用來舉例的範例)

以下是[官方](https://docs.microsoft.com/en-us/dotnet/core/tools/global-tools#find-a-tool)提供找尋工具的列表或網站

1. https://github.com/natemcmaster/dotnet-tools

2. https://www.toolget.net/

## 二、專案篇

### 1. 專案類型:此處僅針對`WebAPI`及`WebJob`這二種類型做說明

- `WebAPI`:這種專案類型在預設專案裡,僅提供`Controller`的範例(建立指令:`dotnet new webapi`),不提供任何有關網頁(動態及靜態)的支援(當然可以事後追加),可以佈署到一般IIS站台,Azure的`App Service`,或是獨立運行(內建Kestrel server)

- `WebJob`:又稱之為`Worker service`,這是一種背景服務的專案型態,適合用來做像是`排程服務`/`監控`/`批次工作`...等!它可以以Windows Service的形態運作([參考這篇](https://medium.com/ek-technology/把worker-service變成windows-service-59845ec1fb17)),若是佈署到Azure則是在`Web Job`的項目之下!(Web Job是在App Service下的功能,可以在一個App Service下佈署多個Web Jobs)

  https://www.stevejgordon.co.uk/what-are-dotnet-worker-services

### 2. 專案組成架構說明:針對專案內的檔案用途做說明

<img style='width:200px' src='assets/demo_solution.png' ></img>

- `servicelib`: 上圖的`servicelib` 是一個程式庫專案`ClasLib`
  
  - `bin`:存放建置後的內容,若要手動執行或佈署,均在這裡找(不建議加入版控)
  
  - `obj`:存放建置過程的中間檔(不建議加入版控)

  - `servicelib.csproj`: [專案管理檔](https://docs.microsoft.com/zh-tw/dotnet/core/project-sdk/overview),這檔案存放此專案相關資訊,建議開發人員要熟悉此內容(早期開發.net專案,均依賴Visual Studio的專理,而不會去檢視/管理這類的檔案,此檔的內容並不複雜,了解後有助於版本控管)
  
    ```xml
    <Project Sdk="Microsoft.NET.Sdk.Web">
        <PropertyGroup>
            <TargetFramework>netcoreapp3.1</TargetFramework>
        </PropertyGroup>
        <ItemGroup>
            <PackageReference Include="NLog" Version="4.7.2" />
            <ProjectReference Include="..\servicelib\servicelib.csproj" />
            <ProjectReference Include="..\webapp2\webapp2.csproj" />
      </ItemGroup>
      <ItemGroup>
	      <Content Update="nlog.config">
	        <CopyToOutputDirectory>Always</CopyToOutputDirectory>
	      </Content>
	  </ItemGroup>
	  </Project>
	  ```

	  (第1行)專案類型專用的SDK,這裡指定  `Microsoft.NET.Sdk.Web`會自動引入Web相關的所有程式庫!
	  
	  (第3行)使用Framework的版本,這裡是指定dotnet core 3.1版,這要搭配本機安裝的SDK版本! **注意**: 這裡的`netcoreapp3.1` 前後均不可以有空格(我在這裡撞牆超久,疑似是個bug)
	  
	  (第5行)這裡是指定安裝的nuget套件,及參考的專案,如果要控管nuget套件,最快的方式就是在這裡手動加入要用的套件,在存檔後,會根據不同的IDE工具,自動將套件下載安裝,若要手動安裝,可輸入`dotnet restore`即可!
	  
	  (第11行)這裡是指定要隨之發佈的檔案(們)
	
- `webapp1`: 右圖的`webapp1` 是一個Web API專案`webapi`


  - `appsetting.json`及`appsetting.Development.json`:是專案設定檔,功能等同於`web.config`,不過此檔案是 json格式

  - `launchSettings.json`: 用來設定專案啟動的參數

    ```json
    {
      "$schema": "http://json.schemastore.org/launchsettings.json",
      "iisSettings": {
        "windowsAuthentication": false,
        "anonymousAuthentication": true,
        "iisExpress": {
          "applicationUrl": "http://localhost:57983",
          "sslPort": 44391
        }
      },
      "profiles": {
        "IIS Express": {
          "commandName": "IISExpress",
          "launchBrowser": true,
          "launchUrl": "weatherforecast",
          "environmentVariables": {
            "ASPNETCORE_ENVIRONMENT": "Development"
          }
        },
        "webapp2": {
          "commandName": "Project",
          "launchBrowser": true,
          "launchUrl": "weatherforecast",
          "applicationUrl": "https://localhost:5001;http://localhost:5000",
          "environmentVariables": {
            "ASPNETCORE_ENVIRONMENT": "Development"
          }
        }
      }
    }
    ```

    (第3-10行)針對IISExpress通用環境的設定

    (第12行)是在啟動時以本機IISExpress來啟動並掛載此專案,這裡的啟動會參照(第3-10行)的設定

    (第20行)是以內建主機(Kestrel server)來啟動

### 3. 專案的垂直分層(Layer):專案裡的分層及各種目錄的習慣分類方式

#### 3-1. 主要分層的目的

  - `展示層`(Presentation Layer): 若是在MVC或是一個包含View的專案下,這是指`View`+`Controller`, 若在Web API的專案時,則是指`Controller`,主要是做為使用者資料的介面(介面不光是指看的到的畫面)的接受及回傳(會對使用者做認証),同時會對資料做格式化(轉換/拆分/合併成所需的資料型態),驗証(非關邏輯的,表層的),並向下一層派送

  - `服務層`:服務層是提供服務的單位(一個服務可以想成是做一件事),並且是開放層的最底層(此層以下的分層就不適合開放給外界使用)!服務層一般來說都是做一件完整的事,會有一段流程(處理順序,流程控制,迴圈...等等),會將資料分割派送給不同業務層...等等!例如: `提交訂單`,就是一個完整流程,但`加入一筆訂單`就不是,因為加入一筆訂單僅是提交訂單過程中的一個步驟,其它可能還會有像是:`檢查庫存`,`檢查客戶紀綠`,`按排出貨`,`依付款方式觸發付款流程`...等等的作業!

    *要注意的是,`服務層`並沒有`規定`只能處理一件完整的事務,使用者介面上要處理的任何事情都有可能對應到服層裡的一個服務,而這時就是把這件事轉交(by pass)到業務層即可!*

  - `業務層`:業務層主要是處理一段最小不可分隔的業務流程,而此段流程可以在一個服務層中組合成一個工作單位,例如:`付款`,這業務流程可以組合在單訂作業中,但它不能單獨發起!
    對於`服務層`及`業務層`的分層目的,主要是取決於整體企業流程的複雜度,並為了讓每個單元流程能被分離解耦進而拆分,一但做到對業務層能拆分清楚,則其可覆用的程度就會變高,當有一個新的服務流程產生時,可以透過可覆用的業務層來快速組合出所有流程,不用再為新的流程去撰寫程式!

    反過來說,如果服務要處理的事務並不複雜,也沒有必要分出業務層,因為分的層越多,執行效能就會受影響,對管理上的問題也不一定好事!

    > 註: 以目前我們的架構,業務邏輯大多放在資料庫裡的Stored Procedure,而沒有這一層!
    
  - `永續層`: 這層包括資料模型(資料的長像),及處理資料的方法,用來對資料模型做[CRUD](https://zh.wikipedia.org/wiki/%E5%A2%9E%E5%88%AA%E6%9F%A5%E6%94%B9)!資料的處理不僅是指對資料庫的處理,任何型態的資料處理,都屬於這一層!

    > 註: 早期對於ADO.NET的封裝,ORM(例如:Entity framework),上擎目前在用的`SSAPI`,均屬於這一層!

  - `資料層`: 用來存放資料獨立層,一般是指資料庫/檔案管理,或是任何能處理資料的機制!

#### 3-2. 各分層的習慣命名方式及原則

- `展示層`:這裡是指控制器,會放在`Controllers`的目錄下(注意大小寫及使用複數型態)

- `服務層:`一般習慣是命名為`Services`,(另外我也見過命名為 `Managers`的,不過並不建議),此層下會再開一個存放介面的目錄`Interfaces`,介面的命名,字首一律加上大寫`I`,例如: `IAppService.cs`

  > 註: 以上二層的命名,普徧用於dotnet的生態系統下,因此建議採用並一至化(尤其是`Controller`層,其名稱一但改變,可能會在預設情形下無法運作!)


- `資料模型`: 一般命名為`Models`,這裡是用來存放各種資料模型,資料模型依其用途,又可再分為以下三類

  - 用來傳遞參數的資料: 這類模型可以放在`Params`的次目錄下,並在類別名稱後帶上`Param`,例如:`BatchPushParam.cs`,這種型態的資料一般是用在`展示層`接收使用者傳來的資料,這類資料的格式比較是以功能資料為導向(以大陸的術語就是:面向使用功能),所有類型的資料可能都包含在一個模型中!

  - 用來在各層之間傳遞的資料: 這類模型可以放在`Dto`的次目錄下,並在類別名稱後帶上`Dto`,例如:`CustomerDto.cs`這種資料模型主要是用來承載服務層所需要的資料格式,相對於展示層的參數資料,會比較細緻化,並更專屬性於單一功能所需要的欄位!

    >  `Dto`是Data Transfer Object的縮寫,這類模型的特點就是在一個類別裡,只有屬性,沒有操作(方法),單純用來存放資料(不做驗証,不做邏輯上的操作)! 也有人稱之為`VO`(Value Object),但是實際上`VO`跟`Dto`是不同的東西(可以參考這篇:https://www.adam-bien.com/roller/abien/entry/value_object_vs_data_transfer)

  - 用來對應資料層的資料: 這種模型,是完整對應來自資料庫的結構,用來存放資料庫裡的部份或全部內容! 若是在Entity framework的使用架構(ORM)下,它可以視為一個`Entity`(實體),這種模型除了存放資料外,還自帶(或由系統自動外掛)各種對資料庫的操作及驗証!可以將相關的檔案放在`Repositories`的目錄下(在此目錄下可能又包含不同種類的類別,再依其性質去細分)

    若是自行處理對資料的維護,可以將這類資料和`Dto`合併使用!

    > 註1: 參數資料其實也是`Dto`,在這裡只是為了使用上而將其區分,在實務上如果不區分,也許會造成命名的困擾!

  資料模型的目錄建議最終如下:

  ```
  \Models
    \Params
    \Dto
  ```

  

  *其它分類目錄的建議:*

- 輔助功能類別: 建議目錄名稱為`Helpers`或`Utilities`,這裡存放所有需要注入的輔助型類別(因為是要注入使用,在此目錄下要對其介面再建一個子目錄`Interfaces`)!輔助類別一般可能是

    - 不易為其做特定的分類且到處都會用到的類別
    - 擴展類別(`Extensions.cs`): 擴展類別的說明請看[這裡](#12. 擴展類別)
    - 第三方提供的原始碼,例如:SSAPI的Client端
    - 其它(還沒想到)

- 靜態網頁檔: 這裡一般會習慣用`wwwroot`來命名,例如:我們專案常用的`ProxyPage` 就適合放在此

- 紀錄檔: 紀錄檔可以統一放在`logs`目錄下

- 資料存放目錄: `App_Data`的相關說明,請參考這裡[13. 資料目錄](#13. 資料目錄)的說明及建議

### 4. 專案的水平拆分(Reference)

####4-1.專案的水平拆分的幾種情形

- 在同一個方案中,如果有多個專案要使用的`服務層`大多相同或相近,可以將`服務層`獨立成一個專案,並由其它專案以`加入參考`方式來引用!`服務層`的可重覆使用性是在系統設計時,優先考量的組合的單位!若規劃得宜,`服務層`是可以做為企業內共用專案的!(注意的是,這是對內部開放的架構)
- 在以對外`服務`為考量的基礎下,以`展示層`(指的是`Controller`,也就是API)來當作服務單位的`集合體`的規劃,主要是考量到實體架設服務的主機分配(效能/流量/位置/安全控管/版本/專案別...等因素)來拆分,一般在一個系統下可能拆分多個專案(Web API)來建置到多處主機上,或是可以彈性將多個專案合併到同一個主機下,或是合併成同一個專案!
  - 合併/拆分是以主機為單位:基本上專案內不需要做什麼改變
  - 合併是以專案為單位:若想維持住原專案的結構,不想做太多的改變,用專案參考專案的方式是最容易的方式!
  - 拆分專案:這只能手動拆分,並依實際情形建立可覆用的`服務層`!

#### 4-2. 案例

在某推播系統,其服務架構圖

```mermaid
 classDiagram
      SSPushCommon <|-- SSMaintainServer
      SSPushCommon <|-- SSPushServer
      SSPushCommon <|-- SSRegisterServer
      <<Service>> SSPushCommon
      class SSMaintainServer{
          
      }
      <<WebAPI>> SSMaintainServer
      
      class SSPushServer{
          
      }
      <<WebAPI>> SSPushServer
      class SSRegisterServer{
          
      }
      <<WebAPI>> SSRegisterServer
```

在這系統裡,有三個`WebAPI`,這些API都在同一個系統下,但為了效能及流量,將其拆分成四個不同的專案:`SSMaintainServer`,`SSPushServer`,`SSRegisterServer`,這三個API可以依照需求來佈署到三台不同的主機,或是同一台主機的不同站台(彈性化佈署)!而第四個專案`SSPushCommon`是一個共用的`服務層`,此層是被引入到不同專案中!

#### 4-3. 在組織一個方案(多個專案)時,可以

   1. 專案之間相互參照(Reference),但要注意不要形成迴圈(A參照B,B參照C,C又參照A)

   2. 建議為`服務層`建立獨立專案(專案類型以`程式庫`型態存在,`dotnet new`可以查看到`classlib`的類型即為此類型)

   3. 若要將`展示層`(這裡是指`Controller`)拆分不同專案,要注意以下幾點

      - 所有專案不可有同名`Controller`存在,以免造成路徑無法對應,或對應錯誤
      - 所有`注入項目`及`中介軟體`要在**主要專案**中設定!因為程式進入點只有一個(即在主要專案裡),被引入的專案的`Program.cs`不會被執行,所以在**被引入的專案裡的所有設定都無效**!(**這點很重要,很重要,很重要!!!**)
      - 早期版本的.net framework, dotnet core 均有機會遇到被引入的`Controller`無法被主要專案找到(應該是bug),如果遇到此問題,則必需視情況處理(找Sam協助)

### 5. 方案的組織

```bash
$(方案目錄)/
  artifacts/
  build/
  docs/
  lib/
  packages/
  samples/
  src/
  tests/
  .editorconfig
  .gitignore
  .gitattributes
  build.cmd
  build.sh
  LICENSE
  NuGet.Config
  README.md
  {solution}.sln
```

- `src` - 各專案放在此目錄下

- `tests` - 各測試專案放在此目錄下(目前未使用)

- `docs` - 所有說明文件檔案及相關的圖片/多媒體檔...等.

- `samples` (optional) - 範例專案(如果此專案是一個程式庫,可以為它寫一個範例)

- `lib` - 如果依賴的程式庫在nuget package裡找不到,可以將其dll檔放此處(所有專案共用,統一版本)

- `artifacts` - 可以考慮把build後的`bin`內容統一轉放在此,比較好找(不用一層一層找到各專案下的bin)

- `packages` - 如果想要將nuget套件包含在方案裡,可以放在此處(依照目前nuget套件,均存放在檔案系統的個人使用者目錄下,並在專案裡設定目標到實際目錄下的檔案,此方式可以有效防止相同套件在各目錄下重複存在,浪費硬碟空間,亦可提升套件下載效率,所有專案僅需下載一次!)此外,會考慮將nuget套在拉回方案目錄下的情形,一般是在要封存完整方案內容時才會如此處理!

- `build` - 如果要為此專案客制化`建置`過程,可以寫一套腳本,並放在此目錄下(可略過)

- `build.cmd` - Windows版本建置的啟動腳本(可略過)

- `build.sh` - Linux版本的建置啟動腳本(可略過)

- `README.md` - 本方案的說明根(可以參照到doc目錄下)文件

- `.editorconfig` - `.editorconfig`的設定,可以協助開發者在不同開發平台/IDE工具,在環境的設定上保持一至性,例如:檔案編碼,縮排(格數,是否用tab...等),行結尾方式(crlf/lf),是否自動去掉尾空白....等等,可以參考: [官方](https://editorconfig.org/),及[微軟給的範例](https://docs.microsoft.com/zh-tw/visualstudio/ide/editorconfig-code-style-settings-reference?view=vs-2019)

- `.gitignore` - 不需進`git`版本控管的設定檔案,實際內容如下(需依實際內容再調整):

  ```
  *.swp
  *.*~
  project.lock.json
  .DS_Store
  *.pyc
  nupkg/

  # Visual Studio Code
  .vscode

  # Rider
  .idea

  # User-specific files
  *.suo
  *.user
  *.userosscache
  *.sln.docstates

  # Build results
  [Aa]rtifacts/
  [Dd]ebug/
  [Dd]ebugPublic/
  [Rr]elease/
  [Rr]eleases/
  x64/
  x86/
  build/
  bld/
  [Bb]in/
  [Oo]bj/
  [Oo]ut/
  msbuild.log
  msbuild.err
  msbuild.wrn
  
  # Visual Studio 2015
  .vs/
  ```



## 三、開發篇

### 1. 啟動程式的過程

#### 1-1. 程式的進入點

C#的程式進入點是`Program.cs`裡的`public static void Main(string[] args)`,這點從以前到現在都沒變:

```c#
public class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) => // => expression body
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
}
```

1. 進入後會透過`Host.CreateDefaultBuilder(args)`來處理一些預設的流程及環境([參考這裡](https://docs.microsoft.com/zh-tw/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-3.1#default-builder-settings)).
   - (第8行)這裡回傳的是`IHostBuilder`,這是目前官方建議用法,不管是不是Web伺服器(像是背景服務),均統一用這個!
   - 有另一種`IWebHostBuilder`的回傳,這是為了相容舊版dotnet core(dotnet core 2.X版)而存在的,並非建議使用!
2. 再來會再呼叫(第10行)`ConfigureWebHostDefaults`來建立一個`Kestrel`為預設web伺服器,若是非web的服務,則應該改用`ConfigureServices()`來建立!
3. (第12行)啟動`Startup.cs`程式來設定或載入一些相關設定,包括相依性插入(DI),中介軟體(Middleware)...等

#### 1-2. `Startup`的啟動

`Startup.cs`負責啟動時期,要設定及載入的所有項目(服務及中介軟體),預設內容如下:

```C#
public class Startup
{
    //建構式,會傳入IConfiguration,並保存起來,這是一個極為重要的物件
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration; 
    }
	public IConfiguration Configuration { get; }

    // DI的部份
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddRazorPages();
    }
      
    // Middleware的部份
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            app.UseHsts();
        }
        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseRouting();
        app.UseAuthorization();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapRazorPages();
        });
    }
}
```

1. (第4行)建構式除了可以傳入`IConfiguration`(其實這裡也是注入)還有另外二種服務(這裡說的服務,就是可注入的類別)

   - [IWebHostEnvironment](https://docs.microsoft.com/zh-tw/dotnet/api/microsoft.aspnetcore.hosting.iwebhostenvironment): 繼承自`IHostEnvironment`, 提供程式在運行環境下的一些相關資訊,例如:`ApplicationName`, `ContentRootPath`...等
   - [IHostEnvironment](https://docs.microsoft.com/zh-tw/dotnet/api/microsoft.extensions.hosting.ihostenvironment): 同上,多了`WebRootPath`,`WebRootFileProvider`的屬性值(編註:可能取不到值)
   - [IConfiguration](https://docs.microsoft.com/zh-tw/dotnet/api/microsoft.extensions.configuration.iconfiguration):取得應用程式組態(即`appsetting.json`)的內容,透過`GetSection('區段名')`來取得區段,再字典方式取值,例如:` Configuration.GetSection("AppSettings")["SSIDS4Url"];`即可取到`AppSettings`這個區段裡的`SSIDS4Url`的值!

   *特別說明: 這三個服務是由系統準備好的,可以不用配置即可直接拿來注入!*

2. (第11行)相依性插入(依賴注入,Depency Inject): 在`StartUp.cs`裡的所謂`服務`,即是指要在專案裡加入的可被注入項目,是由`ConfigureServices()`這個方法內處理,加入或客製!(註:這部份一般來說沒有順序性)
3. (第17行)另一個部份是管線處理,也就是中介軟體的部份,是由`Configure()`這個方法內處理(註:這裡是有順序性的,順序不同,是有可能造成錯誤!)

### 2. 相依性插入(或稱依賴注入)

#### 2-1. 何謂依賴?

在下圖中,`CustomerController`裡的`GetAllCustomers()`API,要取得資料庫裡的所有客戶資料,在MVC分層處理的架構下,我們會設計一個Service層,用來做邏輯及資料的處理,那我們可以說`CustomerController`依賴`CustomerService`(注意這裡的方向,是由Controller指向Service)

```mermaid
classDiagram
      CustomerController ..> CustomerService : 依賴
      CustomerController : +IActionResult GetAllCustomers()
      CustomerController : -CustomerService customerService
      class CustomerService{
          +Customer[] GetCustomers()
      }

```

這種依賴模式,是最傳統的方式,由需求方(`CustomerController`)建立一個被依賴方的物件,再去使用後者提供的相關方法(這裡重點是:需求方要*建立*被依賴的物件),這種方法會引起以下問題:

1. 在所有的需求方,均要自己建立一次被依賴物件,被依賴方的實作若要改變,只能去改`CustomerService`內容,無法用抽換方式(實作不能代換)
2. 若一定要代換實作(例如:把`CustomerService`改為`MyCustomerService`),要將所有依賴方的程式碼改掉,也不能保証`CustomerService`及`MyCustomerService`之間是否有簽名不同的問題(可加入介面來抽象化,改善此問題)
3. 相同的,為了要隨時抽換被依賴方的實作物件,每次都要改程式碼,也不利於做單元測式(執行時可決定要實作的類別)!(無法改用Mock物件來以假資料替換)
4. 被依賴方的實作建立過程,如果較為複雜(但一至),若在各需求方都自行撰寫建立過程,也難保不會有其中一部份有寫錯(或寫出差異)的情形

#### 2-2. 加入介面

```mermaid
classDiagram
      CustomerController ..> ICustomerService : 依賴(自行建置)
      ICustomerService <|.. CustomerService : 實作
      ICustomerService <|.. MyCustomerService : 實作
      CustomerController : +IActionResult GetAllCustomers()
      CustomerController : -ICustomerService customerService
      class ICustomerService{
          +Customer[] GetCustomers()
      }
      <<interface>> ICustomerService
      class CustomerService{
		      +Customer[] GetCustomers()  
      }
      class MyCustomerService{
          +Customer[] GetCustomers()
      }
```

如上圖所示,加入介面後,被依賴的部份由`CustomerService`改為`ICustomerService`,需求方可以任意建立出實作的類別!但是,現在並未解決建立物件要在執行時再決定要用那一個實作!

#### 2-3.依賴反轉(IoC, Inverse of Control)

```mermaid
classDiagram

      ICustomerService <|.. CustomerService : 實作
      CustomerController ..> ICustomerService : 依賴(僅宣告)
			CustomerController <.. CustomerService : 注入(環境)
      CustomerController : +IActionResult GetAllCustomers()
      CustomerController : -ICustomerService customerService
      class ICustomerService{
          +Customer[] GetCustomers()
      }
      <<interface>> ICustomerService
      class CustomerService{
		      +Customer[] GetCustomers()  
      }
      
```

對於被依賴的類別,需求方僅宣告一個介面,只要實作此介面的類別均可在執行環境中由系統來建立(建立過程可客製),並注入到需求方中(需求方的宣告是在建構式中透過參數方式,由系統自動傳入)

> 註: 對"依賴"這件事,在物件導向設計的觀點來看是不佳的!因為依賴程度高,代表耦合度高,也代表重用(Reuse)度就降低!依賴反轉可以把依賴解除(解耦),使其重用的程度提高!

#### 2-4.程式碼實作過程

1. 以一個`AppService`為例,`IAppService`的介面:

   ```c#
   public interface IAppService
   {
       Task<AppDTO[]> GetAllApps();
       Task<AppDTO> GetApp(int appId);
       Task<AppVersionDTO> GetAppVersion(int appId, int appVersionId);
       Task<AppMessageVersionDTO[]> GetAppVersionsByMessageId(long messageId);
       Task<AppMessageVersionDTO[]> GetAppVersionsByBatchNo(int batchNo);
   }
   ```

2. `AppService`實作(這部份沒有什麼特別的,就是一般的實作,因此不列出完整內容)

  ```c#
  public class AppService : SSApiBaseService<AppService>, IAppService
  {
      public AppService(IHttpContextAccessor accessor) : base(accessor)
      {
      }

      public async Task<AppDTO[]> GetAllApps()
      {
          // 略...
      }
      // 下略
  ```

3. 在`StartUp.cs`中,加入DI的建立

  ```c#
  public class Startup
  {
      public Startup(IConfiguration configuration)
      {
      Configuration = configuration;
      }

      public IConfiguration Configuration { get; }

      public void ConfigureServices(IServiceCollection services)
      {
      services.AddHttpContextAccessor();
      services.AddScoped<ICommonHelper, CommonHelper>();
      services.AddScoped<ISSApiClient, SSApiClient>();
      services.AddScoped<IAppService, AppService>();
      // 中間略...
      services.AddSingleton<IMemoryCache, MemoryCache>(); 
}
  ```

​	(第15行)在加入服務的地方,將`AppService`的介面及實作以`AddScoped`的方式加入!

4. 在`AppController`中

   ```c#
    public class AppController : SSApiBaseController<AppController>
    {
        private readonly IAppService _appService;
   
        // 由建構式注入appService,這個appService是一個實體物件
        public AppController(IAppService appService, IHttpContextAccessor accessor) : base(accessor)
        {
            _appService = appService;
        }
   
        [HttpGet]
        [HttpPost]
        public async Task<IActionResult> GetAllApps()
        {
            LogEntered();
            try
            {
                var result = await _appService.GetAllApps();  // _appService直接使用
                LogDone(result);
                return ResponseOk(result);
            }
            catch (Exception ex)
            {
                LogException(ex);
                return ResponseError(ex);
            }
        }
     // 略...
   ```

   (第6行)系統會在`AppController`被建立時,自動注入可被注入的類別項目,透過建構式的參數傳入(**這裡的參數順序可以任意排列**)

   (第18行)在整個Controller裡並沒有任何地方去做`new AppService()`,而這裡直接可以使用,由此証明此物件是真實被注入的!

#### 2-5.加入注入的`AddScope`是什麼?(生命週期)

在注入時,dotnet執行環境會在啟動時建立宣告的服務項目(各個要注入的類別物件),而這些物件的初始化方式,會依照實際需要,分為三種初始化物件的生命週期:

1. `Transient`: 在每個需求者需要注入時,都會為其注入一個不同的依賴實體,並在需求者結束時會同時將被依賴者做結束處置(dispose),加入的語法: `services.AddTransient<注入的介面名稱,注入的實體類別名稱>(自訂實體化過程)`
2. `Scoped`: 在同一個**前端請求**(http request)的過程中,所有被依賴的實體均用同一個,請求結束時會將依賴者做結束處置(dispose),加入的語法: `services.AddScoped<注入的介面名稱,注入的實體類別名稱>(自訂實體化過程)`
3. `Singleton`: 被依賴實體從程式一開始到結束,完全都是同一個(單體),加入的語法: `services.AddSingleton<注入的介面名稱,注入的實體類別名稱>(自訂實體化過程)`

以下是一些常用範例:

```C#
services.AddTransient<IAppService, AppService>(); // 標準建立過程
services.AddScoped<IAppService>(o => new AppService("建構式參數內容")); // 自己客製建立實體,實體化方式為一個lambda表達示
services.AddScoped<AppService>(); // 如果要注入的類別沒有介面,也是可以注入,不過若是自己設計的類別,不建議如此使用
services.AddSingleton(new AppService()); // 客製實體是在當下就建立,這只能用在`Singleton`的生命週期
```

> 註: Lambda表達示在這裡的作用,主要是使其客製建立的過程延後到注入時才(執行)實體化
>

#### 2-6. 各種生命週期的補充說明:

 1. `Singleton`的使用時機及注意事項:
- 處理大家共用的資料且只能有一份的情形,例如:共用快取,記錄(例:NLog),系統層級的選項(參數)資料,共用連線(HttpClient)使其不會產生大量連線數...等等
   - 由於大家都要共用,因此要注意thread-safe的問題(thread-safe的必要條件),簡單說就是大家一起呼叫時,要能控管資料完整及一至性問題(鎖定及死結)
   - 此類別效能問題,也許會變成瓶頸
 2. `Transient`
    - 當注入物件很小,且沒有長時/高負荷運作(例如:只有運算),但需要紀錄個別狀態時(如果不需要紀錄狀態,用Singleton比較適當)
    - 要注意產生大量物件的問題,尤其是在前端有大量請求(http request)時,大量物件會造成系統記憶體不足,效能變差,甚至崩饋

 3. `Scoped`
    - 在無狀態(或狀態僅在一次請求內有效)但長時/高負荷運作的情形下,用Scoped是較佳的情形,也是一般情形下預設使用的生命週期

### 3. 中介軟體管道(middleware pipeline)

在一個請求(http request)進來時,可以在控制器接收到請求之前,加入前置處理,例如:認証/授權檢查,參數正規化,路由選擇,系統紀錄,跨網域設定,重導,存取靜態資料,例外處理...等等,這些中介功能是由一個一個中介程式串連起來並接序運行的(其順序可能會影響最終結果)

```mermaid
sequenceDiagram
		participant Response
		participant Request
		participant Middleware1
		Note right of Middleware1: 中介軟體
    participant Middleware2
    participant Action
    Note right of Action: 控制器裡的方法
    Request->>Middleware1: 
    Middleware1->>Middleware2: next()
    Middleware2->>Action: next()
		Action-->>Middleware2: return
		Middleware2-->>Middleware1: return
		Middleware1-->>Response: return   
```

一個簡單的例子:

```C#
public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        // app.Run不會再呼叫下一個middleware
        app.Run(async context =>
        {
            await context.Response.WriteAsync("Hello, World!");
        });
    }
}
```

在這例子中`app.Run()`其實是一個`middleware`(也是這例中唯一的),它在接收到前端請求後,會執行參數裡的lambda表達示(第5行),而表達示裡則一律在回應(`Response`)裡寫入一個"Hello world!"的字串,因此這個Web站台,不論怎麼呼叫,回應都是此字串!

```c#
public class Startup
{
    public void Configure(IApplicationBuilder app)
    {
        // 第一個middleware
        app.Use(async (context, next) =>
        {
            // 在進入下一個middleware之前要處理的事務
            await next.Invoke();  // 進入下一個middleware,在這之前的程式,切勿更改Response的內容
            // 由下一個middleware結束後要處理的事務
            await context.Response.WriteAsync("Hello 1.\n");
        });
				// 第二個middleware,app.Run()要放在最後
        app.Run(async context =>
        {
            await context.Response.WriteAsync("Hello 2.\n");
        });
    }
}
```

在這例子中,`app.Use()`類似`app.Run()`,但它可以呼叫下一個middleware(第9行),此例執行後的結果如下(留意其順序):

```
Hello 2.
Hello 1.
```

若要依據請求的網址來處理,可以用`app.Map()`或`app.MapWhen()`

```C#
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
    // 第一個參數是比對完整的路徑
    app.Map("/api/hello", (appBuilder) =>
    {
        appBuilder.Run(async (context) => {
            await context.Response.WriteAsync("hello world!");
        });
    });
    // 以條件是否符合來決定是否要處理(此功能較強大)
    app.MapWhen(context => context.Request.Query.ContainsKey("world"), (appBuilder) =>
    {
        appBuilder.Run(async (context) =>
        {
            await context.Response.WriteAsync("你在hello!");
        });
    });
}
```

#### 3-1. 預設dotnet core的web api專案啟動內容說明

```c#
public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }
    public IConfiguration Configuration { get; }
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
    }
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        app.UseHttpsRedirection();
        app.UseRouting();
        app.UseAuthorization();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
```

(第10行)這會自動掃描所有的控制器,並自動加入為注入項目

(第16行)在開發模式下(參考環境變數篇說明),會使用內建的錯誤訊息頁面,提供開發者方便除錯(在這裡可以加入else項,把錯誤訊息重導到自訂頁面!)參考: https://docs.microsoft.com/zh-tw/aspnet/core/fundamentals/error-handling?view=aspnetcore-3.1

(第18行)這會將非https的請求,強制重導到https請求上,若想要將其分開,就將此行移除(remark)掉

(第19行)使用dotnet提供的路由功能,一般均會使用

(第20行)啟用dotnet提供的授權機制,用來控管哪些控制器可以被執行(資源可用範圍)

(第21行)啟用端點對應功能,這裡會將端點url對應到控制器上,至於對應到那一個控制器,可以在各控制器上指定其端點名稱對應規則

以下是[官方](https://docs.microsoft.com/zh-tw/aspnet/core/fundamentals/routing?view=aspnetcore-3.1#routing-basics)對於`UseRouting()`及`UseEndpoints()`的說明:

- `UseRouting()`將路由對應新增至中介軟體管線。 此中介軟體會查看應用程式中定義的端點集合，並根據要求選取[最符合的項](https://docs.microsoft.com/zh-tw/aspnet/core/fundamentals/routing?view=aspnetcore-3.1#urlm)。
- `UseEndpoints()`將端點執行新增至中介軟體管線。 它會執行與所選端點相關聯的委派。

#### 3-2.各種擴充例子

- 3-2-1. 取得`HttpContext`: 若想要取得`HttpContext`,可以注入`HttpContextAccessor`,注入方法如下:

  ```c#
  services.AddHttpContextAccessor();
  ```
  
  在需求端裡可以注入此項目,來取得`HttpContext`
  
  ```c#
   public SSApiBaseService(IHttpContextAccessor accessor)
   {
       _config = accessor.HttpContext?.RequestServices.GetService<IConfiguration>();
       _cache = accessor.HttpContext?.RequestServices.GetService<IMemoryCache>();
       _env = accessor.HttpContext?.RequestServices.GetService<IWebHostEnvironment>();
       _logger = accessor.HttpContext?.RequestServices.GetService<ILogger<T>>();
       _commonHelper = accessor.HttpContext?.RequestServices.GetService<ICommonHelper>();
       _ssApiClient = accessor.HttpContext?.RequestServices.GetService<ISSApiClient>();
   }
  ```
  
  這段範例其功能是不透過自動注入,以`GetService()`取得注入項目,不過在官方不建議如此使用,如果可以自動注入,建議使用自動注入!(這裡會如此使用是有其它原因)

- 啟用跨原始來源要求(跨原始來源設定並非只有此一方式, [完整官方說明在此](https://docs.microsoft.com/zh-tw/aspnet/core/security/cors?view=aspnetcore-3.1))

  注入是提供給middleware用的:

  ```c#
  services.AddCors(options =>
  {
      options.AddDefaultPolicy( // 設定DefaultPolicy
          builder =>
          {
              builder.AllowAnyOrigin() // 允許所有來源
                  .AllowAnyHeader()    // 允許所有標頭
                  .AllowAnyMethod();   // 允許所有請求方法
          });
  });
  ```

  middleware(官網裡有使用自訂名稱的用法)

  ```C#
  app.UseCors();  // 若不給參數,會用DefaultPolicy的設定(DefaultPolicy並非預設存在,若未設定,則完全沒有作用)
  ```

- 對控制器的擴充

  在某些nuget套件在使用時,是針對注入項目去擴充,一個典型的案例是`Newtonsoft Json`(要先安裝此nuget套件),這是一個用來處理json的著名套件,如果想要在控制器裡針對請求時傳來/回傳的參數做json轉換,可以如下設定:

  ```c#
  services.AddControllers()
          .AddNewtonsoftJson(); // 注意:這要寫在AddControllers()之後,不可寫成services.AddNewtonsoftJson()
  ```

  在dotnet core裡開始提供一個處理json的程式庫(`System.Text.Json`),但到目前為止,功能很陽春,如果想用原本就習慣的`Newtonsoft.Json`,其覆蓋方式就如上述,但是這裡若不另外設定,預設會將傳入及傳回的屬性名稱字首改為小寫,以符合json的習慣命名方式(小寫駝峰),若不希望做此轉換,可以下方給參數:

  ```c#
  services.AddControllers()
      .AddNewtonsoftJson(option =>
      {
          option.SerializerSettings.ContractResolver = new DefaultContractResolver();
      });
  ```

- 加入`OpenApiDocument` ([Swagger詳細設定](https://docs.microsoft.com/zh-tw/aspnet/core/tutorials/getting-started-with-nswag?view=aspnetcore-3.1&tabs=visual-studio))

  由於[Swagger](https://swagger.io/)做的太棒了,它開始提供了一些文件標準規格,且被dotnet core採用,詳細說明可參考這裡: https://swagger.io/specification/

  如果要在Web專案中,提供swagger文件功能,設定方式如下:

  1. 安裝套件: `NSwag.AspNetCore`及`NSwag.Annotations`

     ```xml
     <PackageReference Include="NSwag.AspNetCore" Version="13.2.0" />
     <PackageReference Include="NSwag.Annotations" Version="13.2.0" />
     ```

  2. 注入設定:

     ```c#
     services.AddOpenApiDocument(config =>
     {
         // 下方設定是可選的
         config.DocumentName = "v1";
         config.Version = Configuration.GetSection("AppSettings")["ApiVersion"];
         config.Title = Configuration.GetSection("AppSettings")["ApiName"];
         config.Description = Configuration.GetSection("AppSettings")["ApiDescription"];
     });
     ```

  3. 中介軟體

     ```c#
     public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
     {
         // ...
         app.UseOpenApi(); // 這是用來產生文件的內容
         app.UseSwaggerUi3(); // 這是用來產生介面
     }
     ```

  啟動應用程式。 瀏覽至：

  - `http://localhost:<port>/swagger` 以檢視 Swagger UI。
  - `http://localhost:<port>/swagger/v1/swagger.json` 以檢視 Swagger 規格。

- `IdentityServer4`驗証設定

  這範例是採用[IdentityServer4](https://identityserver4.readthedocs.io/en/latest/index.html)(以下簡稱ids4)來當做驗証主機(驗証後發access token給前端),只要在middleware裡設定如下:

  ```c#
  services.AddAuthentication("Bearer")
      .AddJwtBearer("Bearer", option =>
      {
          // IdentityServer4的主機url
          var ids4Url = Configuration.GetSection("AppSettings")["SSIDS4Url"];  
          // 本服務的代碼(資源範圍)
          var ids4Audience = Configuration.GetSection("AppSettings")["SSIDS4Audience"]; 
          option.Authority = ids4Url;
          option.RequireHttpsMetadata = false;
          option.Audience = ids4Audience;
      });
  ```

  設定好後,當前端發出http request來請求一個API時(假設此API需要通過驗証才能存取),必需帶一個bearer token,並會自動將此token發給ids4去做(身份)驗証,符合身份後才能執行!

  > 註1: 關於[Jwt](https://jwt.io/)(JSON Web Token)簡單說明: 在原始的oAuth 2.0規範裡,並沒有任何對於token的格式做定義(RFC 6750),也沒有要求在token裡要帶入(carry in)相關資訊,只需要是一段字串,能讓驗証主機知道其身份即可!但在實務上,如果能從token裡取得相關資訊,會方便許多,不需要每次都去驗証主機裡查,因此就有jwt的誕生(RFC 7519),它分成三個部份:`HEADER`/`PAYLOAD`/`VERIFY SIGNATURE`,可以到官網上了解詳細內容!(https://jwt.io/ 及 https://auth0.com/docs/tokens/concepts/jwts)

  > 註2:[IdentityServer4](https://identityserver4.readthedocs.io/en/latest/)是一套Open ID Connect及oAuth 2.0的整合套件(github上的開放源始碼:https://github.com/IdentityServer/IdentityServer4)
  ![shadow-現代化Web API的架構圖](https://identityserver4.readthedocs.io/en/latest/_images/protocols.png)
  在每一個Web API的端點,都要有一個驗証/授權機制來保護其資源,而`IdentityServer4`則提供了一個簡單接入的程式庫,及一個強大的驗証主機

### 4. 環境組態管理

#### 4-1. 環境名稱

環境 API 可以讀取執行程式的環境資訊，例如：環境名稱、網站實體路徑、網站名稱等。
其中環境名稱就是用來判斷執行環境的主要依據，環境名稱是從系統變數為 `ASPNETCORE_ENVIRONMENT` 的內容而來。

ASP.NET Core 預設將環境分為三種：

- **Development**：開發環境
- **Staging**：暫存環境(測試環境)
- **Production**：正式環境

要取得系統變數 `ASPNETCORE_ENVIRONMENT`，可以透過注入 `IHostingEnvironment` API。範例如下：

```C#
public class Startup
{
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
        env.EnvironmentName = EnvironmentName.Development;
        if (env.IsDevelopment()) {
           // Do something...
        }
        app.Run(async (context) =>
        {
            await context.Response.WriteAsync(
                $"EnvironmentName: {env.EnvironmentName}\r\n"
              + $"IsDevelopment: {env.IsDevelopment()}"
            );
        });
    }
}
```

網站啟動時，`IHostingEnvironment` 會從系統變數 `ASPNETCORE_ENVIRONMENT` 取得資料後，填入 `EnvironmentName`，該值也可以從程式內部直接指派。

> 註: 環境名稱並沒有特定的限制，它可以是任意的字串，不是只能用預設的三種分類。

例如自訂一個 **Test** 的環境。如下：

```C#
public class Startup
{
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
        env.EnvironmentName = "Test";
        if (env.IsDevelopment()) {
           // Do something...
        } else if (env.IsEnvironment("test")) {
           // Do something...
        }
        app.Run(async (context) =>
        {
            await context.Response.WriteAsync(
                $"EnvironmentName: {env.EnvironmentName}\r\n"
              + $"This is test environment: {env.IsEnvironment("test")}");
        });
    }
}
```

> 註: 建議判斷環境透過 `env.IsEnvironment("EnvironmentName")`，`IsEnvironment()` 會忽略大小寫差異。

####4-2.組態設定

組態設定檔可以在不同環境都各有一份，或許大部分的內容是相同的，但應該會有幾個設定是不同的。如：資料庫連線字串。
環境名稱也可以跟組態設定檔混用，利用組態設定檔後帶環境名稱，作為不同環境會取用的組態設定檔。

例如：
有一個組態設定檔為 *settings.json*，內容如下：

```json
{
  "SupportedCultures": [
    "en-GB",
    "zh-TW",
    "zh-CN"
  ],
  "CustomObject": {
    "Property": {
      "SubProperty1": 1,
      "SubProperty2": true,
      "SubProperty3": "This is sub property."
    }
  },
  "DBConnectionString": "Server=(localdb)\\mssqllocaldb;Database=MyWebsite;"
}
```

正式環境也建立一個名稱相同的檔案，並帶上環境名稱 `settings.Production.json`，內容如下：

```json
{
  "DBConnectionString": "Data Source=192.168.1.5;Initial Catalog=MyWebsite;Persist Security Info=True;User ID=xxx;Password=xxx"
}
```

載入組態設定方式：

```C#
public class Program
{
    public static void Main(string[] args)
    {
        BuildWebHost(args).Run();
    }

    public static IWebHost BuildWebHost(string[] args)
    {
        return WebHost.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((hostContext, config) =>
            {
                var env = hostContext.HostingEnvironment;
                config.SetBasePath(Path.Combine(env.ContentRootPath, "Configuration"))
                    .AddJsonFile(path: "settings.json", optional: false, reloadOnChange: true)
                    .AddJsonFile(path: $"settings.{env.EnvironmentName}.json", 
                                 optional: true, reloadOnChange: true);
            })
            .UseStartup<Startup>()
            .Build();
    }
}
```

讀取組態設定檔時，會先讀取 *settings.json* 並設定 `optional=false`，指定該檔為必要檔案；再讀取 *settings.{env.EnvironmentName}.json* 檔案。 組態檔載入的特性是當遇到 Key 值重複時，後面載入的設定會蓋掉前面的設定。

以此例來說，當 *settings.Production.json* 載入後，就會把 *settings.json* 的 DBConnectionString 設定蓋掉，而 *settings.json* 其它的設定依然能繼續使用。

> 以上參考來源: https://blog.johnwu.cc/article/ironman-day16-asp-net-core-multiple-environments.html
>
> 環境變數的設定,可以參考此文章裡的說明!

## 四、差異篇(改動/加強/升級)

### 升級: https://edi.wang/post/2018/10/31/migrating-old-aspnet-applications-to-net-core

### 1. 設定檔格式由`webconfig.xml`改為`appsettings.json`

原本的預設設定檔`web.config`改為`appsettings.json`,若要存取其內容,可以注入`IConfiguration`,透過此類別來存取,以下是一些範例:

`appsettings.json`的內容

```json
{
  "AppSettings": {
    "ApiName": "SSPushServer.Core",
    "ApiDescription": "SSPushServer.Core Web API",
    "ApiVersion": "v0.1.1",
    "SSApiUrl": "https://ssapiservice.azurewebsites.net",
    "SSApiDatabaseServer": "sspushdatabaseserver.database.windows.net",
    "SSApiDatabase": "PUSH_CORE",
    "SSApiLogin": "SSsys",
    "SSApiPassword": "xxxxxxxxx",
    "SSIDS4Url": "https://ssidentityservice.azurewebsites.net",
    "SSIDS4ClientId": "sspushserver",
    "SSIDS4ClientSecret": "yyyyyyyyy",
    "SSIDS4Scope": "ssapi",
    "SSIDS4Audience": "sspush",
  },
  "LocalDbApi": {
    "LocalDbApiUrl": "https://sspushlocaldbcore.azurewebsites.net"
  },
  "AzureSettings": {
    "ClientId": "11111111-6bbe-444e-8c4b-22222222",
    "TenantId": "33333333-0651-4e30-9f3f-44444444444",
    "ClientSecret": "L=19eB[u711o1ZgAw6K=hkP2Y_F520U/",
    "SubscriptionId": "5555555-b5f7-481b-8b42-666666"
  },
  "Logging": {
    "IncludeScopes": false,
    "LogLevel": {
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*"
}
```

1. 先取區段,再多次取用其鍵值


```c#
var azureConfig = _config.GetSection("AzureSettings");
var clientId = azureConfig["ClientId"];
var tenantId = azureConfig["TenantId"];
```

2. 直接取值

```c#
var title = _config["LocalDbApi:LocalDbApiUrl"]; // 直接取值
var name = _config.GetSection("AppSettings:ApiName").Value;  // 用冒號分隔區名及值名
var _sessionWS = _config.GetSection("AppSettings")["SSApiDatabase"]; 
```

3. 利用GetChildren及Exists處理所有區塊裡的"鍵":"值": 

```c#
public ContentResult OnGet()
{
    string s = null;
    var selection = Config.GetSection("AzureSettings");
    if (!selection.Exists())
    {
        throw new System.Exception("AzureSettings does not exist.");
    }
    var children = selection.GetChildren();

    foreach (var subSection in children)
    {
        int i = 0;
        var key1 = subSection.Key + ":key" + i++.ToString();
        var key2 = subSection.Key + ":key" + i.ToString();
        s += key1 + " value: " + selection[key1] + "\n";
        s += key2 + " value: " + selection[key2] + "\n";
    }
    return Content(s);
}
```

### 2. nuget套件

在安裝nuget套件是,要留意該套件是否支援`.NET Core`或`.NET Standard`,如果不支援,還是可以安裝,但在編譯時可能會遇到警告或錯誤,或是只能在`Windows`平台上運行! (`.NET Standard`這類的套件,同時支援`.NET Core`及`.NET Framework 4.6.1+`,目前最佳的使用版本是`.NET standard 2.0`)

### 3. App_Data目錄被移除

存放存資的目錄,必需要自行在程式中設定:

```C#
public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
    // set
    string baseDir = env.ContentRootPath;
    AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(baseDir, "App_Data"));

    // use
    var feedDirectoryPath = $"{AppDomain.CurrentDomain.GetData("DataDirectory")}\\feed";
}
```





## 五、WebAPI應用程式

### 1. [ControllerBase](https://docs.microsoft.com/zh-tw/dotnet/api/microsoft.aspnetcore.mvc.controllerbase?view=aspnetcore-3.1)類別

在WebAPI的控制器,一律繼承自`ControllerBase`類別,其它像是MVC/Razor等專案使用的控制器為`Contoller`,WebAPI切勿繼承自`Controller`,因為這個類別會加入提供對於`View`的的支援,供網頁處理,並不適合單純回傳資料的WebAPI來繼承!

```c#
[ApiController]
[Route("[controller]")]
public class WeatherForecastController : ControllerBase
```

### 2. 回傳

`Controller`回傳,可以僅回傳值,模型物件或其它(例如:MVC的View),例如:

```c#
[HttpGet]
public FileModel Get()
{
    return new FileModel
    {
        fileString = _fileService.ReadText("data.txt")
    };
}
```

但是http request一般會需要不同的狀態狀來表示其處理結果,例如:

- `200`:代表正常完成,以`Ok(Object)`回傳
- `400`:代表處理過程有問題,以`BadRequest(Object)`回傳
- `401`:未授權存取,以`Unauthorized(Object)`回傳
- `404`:找不到此資源,以`NotFound(Object)`回傳
- `xyz`:以`StatusCode(int, Object)`可以自訂回傳碼

以下範例回傳一個`401`的Status Code,但依然能回傳其值!這裡使用的是`IActionResult`當作回傳值的型別(這是一個介面),而上述所有的回傳方法均有實作`IActionResult`,因此可以正常回傳!

```c#
[HttpGet]
public IActionResult GetBad()
{
    var file = new FileModel
    {
        fileString = _fileService.ReadText("data.txt")
    };
    return BadRequest(file);
}
```

`IActionResult` 是回傳的最上層型別,其繼承關係之一:

```mermaid
classDiagram
    class IActionResult{
      <<interface>>
    }
    class BadRequestObjectResult{
      此為BadRequest的回傳型別
    }
    class JsonResult {
    
    }
    IActionResult <|-- ActionResult
    ActionResult <|-- ObjectResult
    ActionResult <|-- JsonResult
    ObjectResult <|-- BadRequestObjectResult 
```

依照其繼承關係,我們可以將上例(第2行)改成如下:

```c#
public ActionResult GetBad()
```

如果你在這個Controller裡,僅需要回傳`ActionResult`,沒有其它實作`IActionResult`的型態,這樣寫是完全沒問題的(這個回傳沒有指定回傳的模型model型別),或是指定模型的型別

```c#
public ActionResult<FileModel> GetBad()
```

這樣寫好處是可以檢查回傳的資料型別,不會發生傳回錯的資料型別的問題!而缺點剛好相反,如果在一個處理API裡,要回傳的的資料型別可能有多種,就不要指定型別!

當然,使用`IActionResult`作為回傳型別(第一個範例),是彈性最大的方式,可以回傳的內容沒有限制,但也沒有任何檢查型別!

### 3. 控制器的屬性(Attribute)

屬性(Attribute)可以用來改變一個類別(這裡針對的是控制器及控制器裡的API方法)的特性/行為/動作...等

```C#
[Authorize]
[ApiController]
[Route("api/[controller]/[action]")]
public class SSApiBaseController<T> : ControllerBase where T : SSApiBaseController<T>
{
    // 略... 
}
```

(第1行)`Authorize`屬性用來說明此控制器裡的方法均需要通過授權驗証

(第2行)`ApiController`屬性用來啟用以下特定行為:

- [屬性路由需求](https://docs.microsoft.com/zh-tw/aspnet/core/web-api/?view=aspnetcore-3.1#attribute-routing-requirement): 即路由路徑的設定可以透過[Route]來設定
- [HTTP 400 自動回應](https://docs.microsoft.com/zh-tw/aspnet/core/web-api/?view=aspnetcore-3.1#automatic-http-400-responses): 參考此說明: https://docs.microsoft.com/zh-tw/aspnet/core/web-api/?view=aspnetcore-3.1#automatic-http-400-responses
- [繫結來源參數推斷](https://docs.microsoft.com/zh-tw/aspnet/core/web-api/?view=aspnetcore-3.1#binding-source-parameter-inference): 可以指定參數來源的屬性`[FromForm]``[FromBody]``[FromQuery]`...等
- [多部分/表單資料要求推斷](https://docs.microsoft.com/zh-tw/aspnet/core/web-api/?view=aspnetcore-3.1#multipartform-data-request-inference): 若未設定`[FromForm]`,來源標頭有指定`multipart/form-data`(也就是html form的提交),會推斷用`[FromForm]`方式
- [錯誤狀態碼的問題詳細資料](https://docs.microsoft.com/zh-tw/aspnet/core/web-api/?view=aspnetcore-3.1#problem-details-for-error-status-codes): 參考此說明: https://docs.microsoft.com/zh-tw/aspnet/core/web-api/?view=aspnetcore-3.1#problem-details-for-error-status-codes

(第3行)`Route`路徑規則套用,此例是url分為三段,第一段的`/api`是固定值(必要的),第二段的`/[controller]`會配對到控制器名稱(不含Controller字眼,例如:ApiController,則名稱為Api),第三段的`/[action]`則會對應到方法名!

以下範例是方法的屬性

```c#
[AllowAnonymous]
[HttpGet]
[HttpPost]
public IActionResult Version()
{
    // 略... 
    return ResponseOk(result);
}
```

(第1行)`AllowAnonymous`: 此方法可以給所有人(不需驗証)呼叫(在此的設定會覆蓋掉Controller上的設定,但僅對此方法有效)

(第2行)`HttpGet`:接受請求的Get方法

(第3行)`HttpPost`:接受請求的Post方法

如果想把參數編入url裡(不是用一般quyer string),可以參考下例:

```c#
[HttpGet("{fileName}/{times}")]
public async Task<FileModel> GetByName(string fileName, int times)
{
    var returnString = "";
    for (var i=0; i < times; i++)
    {
        returnString += await _fileService.ReadTextAsync(fileName);

    }
    return new FileModel
    {
        fileString = returnString
    };
}
```

而要傳參數時: `https://localhost:5001/filedemo/getbyname/data.txt/2`, `data.txt`會代入`fileName`變數中,`2`則會代入`times`變數中(注意這是順序性的)

## 六、觀念篇

### 1.  字串格式化

在C#第6版開始,可以使用更方便的字串格式化方式,稱之為字串插值(String interpolation),這種方式不僅用法簡潔,且更具可讀性,因此會建議一律採用此方法來組合/格式化字串(忘了以前的種種複雜)

#### 1-1. 複合格式字串(Composite String)方式:

```c#
string name = "Mark";
var date = DateTime.Now;
// Composite formatting:
Console.WriteLine("Hello, {0}! Today is {1}, it's {2:HH:mm} now.", name, date.DayOfWeek, date);
```

這種方式即為把`name`,`date.DayOfweek`,`date`這三個值分別代入`{0}`, `{1}`, `{2}`這三個佔位符(placeholder),這種使用方式,首先要確認`Console.WriteLine()`方法是否支援此用法,若不支援此方法則要用[String.Format()](https://docs.microsoft.com/zh-tw/dotnet/api/system.string.format?view=netcore-3.1)來轉換!(支援此用法的相關說明:https://docs.microsoft.com/zh-tw/dotnet/standard/base-types/composite-formatting)

#### 1-2. 字串插值用法:

```c#
string name = "Mark";
var date = DateTime.Now;
// String interpolation:
Console.WriteLine($"Hello, {name}! Today is {date.DayOfWeek}, it's {date:HH:mm} now.");
```

在字串前使用錢字符號(`$`)做為前導,在字串裡即可以直接以`{變數名稱}`來插補(也就是替換)!

插補完整格式: `{變數式[,對齊值][:格式化字串]}`

- `變數式`: 即為一個變數或一段計算式(要能轉換成字串)
- `對齊值`(可選): 正數代表靠右,負數代表靠左,而其值代表所佔用的格數,例如: -7, 代表此字串會佔7格,並靠左,不足的部份以空格補!
- `格式化字串`(可選): C#提供了非常豐複的格式化方法(可參考[附註篇](#1.各種字串格式化),這整理包裡用的是舊式的複合式字串,因此實際在用時要把`0`移除,例如第一個項目的貨幣,就要改成`{amount:C}`)

#### 1-3. 插入字串裡的特殊字元

若要在插入字串所產生的文字中包含大括弧 "{" 或 "}"，請使用雙大括弧 "{{" 或 "}}"。 如需詳細資訊，請參閱[逸出大括號](https://docs.microsoft.com/zh-tw/dotnet/standard/base-types/composite-formatting#escaping-braces)。

因為冒號 (":")　在內插補點運算式項目中具有特殊意義，為了在內插補點運算式中使用[條件運算子](https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/operators/conditional-operator)，請用括弧括住運算式。

下列範例示範如何在結果字串中包含大括弧以及如何在內插補點運算式中使用條件運算子：

```c#
string name = "Horace";
int age = 34;
Console.WriteLine($"He asked, \"Is your name {name}?\", but didn't wait for a reply :-{{");
Console.WriteLine($"{name} is {age} year{(age == 1 ? "" : "s")} old.");
// Expected output is:
// He asked, "Is your name Horace?", but didn't wait for a reply :-{
// Horace is 34 years old.
```

插值逐字字串以`$`字元後跟`@`字元開頭。 如需逐字字串的詳細資訊，請參閱[字串](https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/builtin-types/reference-types)和[逐字識別碼](https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/tokens/verbatim)主題。

### 2. 日期處理

#### 2-1 `DateTime`, `DateTimeOffset`, `TimeSpan`, `TimeZoneInfo`之間的選擇(差別)

官方說明:https://docs.microsoft.com/zh-tw/dotnet/standard/datetime/choosing-between-datetime

- `DateTime`的使用,可以用來反映下列情況

  - 僅表達日期,時間不重要

  - 僅表達時間,日期不重要

  - 獨立的日期和時間資訊,不與其它時間或地區有密切相關(例如:某家店在早上9點開始營業)

  - 需要對日期和時間做計算(例如:再過5天是幾月幾日星期幾?)

  - 日期不包括時區或是只表達UTC時間

  - 用來存放由外部取得的日期及時間格式,例如:由SQL Database傳來的日期時間資料

    (上述詳細說明可見官方說明)

- `DateTimeOffset`時類別除了包括`DateTime`本身之外,同時包括了該時間與UTC之間的差異(位移)值,此種日期時間值不會產生模擬兩可的情形(例如:在台北可以代表GMT+8,資料傳到倫敦後又變成GMT+0)

  - 以此產生的日期時間,可以正確定義出唯一的(不模擬兩可的)日期時間點
  - 可以處理日期時間的各種運算
  - 可以用來表達各種時區時間,前題是要給它當前時間及與utc的時間差(分別以`DateTime`及`TimeSpan`表示)
  - 可以存放帶有"日曆"定義的日期,例如:民國年,農曆...等

  > 官方建議:系統裡最好都用這種日期時間來處理!

- `TimeSpan`: 代表一段時間,或二個時間之間差異值(是為一純量),參考下方(2-4)說明
- `TimeZoneInfo`: 這是用來提供時區相關的資訊及操作,可以參考: https://docs.microsoft.com/zh-tw/dotnet/standard/datetime/instantiate-time-zone-info 的說明

#### 2-1預設的日期(時間)轉成字串:

`DateTime.Parse`會盡可能的把各種日期字串,轉成日期物件(`DateTime`)

```c#
string dateString = "1/2/2018 11:22:12 +08:00";
var dateTime = DateTime.Parse(dateString, CultureInfo.CurrentUICulture);
Console.WriteLine(dateTime);	
```

測試過的字串如下(下表不是全部結果):

- "2018-01-01 20:11:22", "2018-1-1 20:1:1": 數字前有沒有補0都沒問題
- "2018-01-01T20:11:22": 中間加'T'也沒問題
- "2008-1-2 11:22:12 +08:00": 後面加上時區調整,+08:00會變成"2008-1-2 03:22:12"

#### 2-2 做一個泛用日期轉換

如果在格式轉換過程中,不確定會有可能遇上什麼怪格式,可以先把可能的格式列在陣列中,再用`DateTime.ParseExact()`方式來轉換,它會試著將陣列中的格式都試著轉一次,若最後還是無法轉,則會產生一個例外(Exception)

```c#
public static DateTime? ToNullableDateTime(this string data)
{
    string[] formaters =
    {
        "yyyy/M/d tt hh:mm:ss",
        "yyyy/MM/dd tt hh:mm:ss",
        "yyyy/MM/dd HH:mm:ss",
        "yyyy/M/d HH:mm:ss",
        "yyyy/M/d",
        "yyyy/MM/dd"
    };
    return DateTime.ParseExact(data, formaters, CultureInfo.CurrentUICulture, DateTimeStyles.AllowWhiteSpaces);
}
```

#### 2-3 將日期與Unix time互轉

日期轉Unix time:

```C#
 public static long ToUnixTime(this DateTime dt)
 {
     return (long) (dt - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
     // return ((DateTimeOffset) dt).ToUnixTimeSeconds();
 }
```

(第3行)是自己算

(第4行)是將日期轉型成`DateTimeOffset`,使用其提供的方法

Unix Time轉回日期

```c#
 public static DateTime? ToNullableDateTime(this long data)
 {
     return DateTimeOffset.FromUnixTimeSeconds(data).LocalDateTime;
 }
```

> 註: Unix time的格式處理可能會在資料庫中遇到

#### 2-4. 日期的運算

##### 2-4-1 二個日期時間的差異計算

`TimeSpan`是一個Struct的結構,可以用來存放一段時間差(二個`DateTime`的差異值,是為一個純量),可以取其`Days`,`Hours`,`Minutes`,`Seconds`,`Milliseconds`,`Ticks`,及以`Total`開頭的這些值,例如:

```C#
DateTime date1 = new DateTime(2008, 12,31, 23,58,59, DateTimeKind.Local);
DateTime date2 = new DateTime(2003, 2,13, 23,50,59, DateTimeKind.Local);
TimeSpan s = new TimeSpan(date1.Ticks - date2.Ticks);

Console.WriteLine(s.Hours);
Console.WriteLine(s.TotalMinutes);
```

- 有加`Total`的(例如:`TotalMinutues`),是二日期時間的總共差異(分鐘)數(會把日期也算進去)
- 沒有加`Total`的(例如:`Minutues`),僅會計算二個時間差(不含日期的部份)

##### 2-4-2 日期時間各值的增減

- `DateTime`及`DateTimeOffset`均提供對各項值的"Add"方法,用來對各值的增減,例如: `dt.AddDays(3)`代表對`dt`日期增加3天,`dt.AddMinutes(-10)`代表對`dt`日期減少3分鐘
- 也可以對`TimeSpan`做增減量計算,例如: `dt.Add(ts)`代表對`dt`增加`ts`的量, `dt.Subtract(ts)`則是做減量計算!(註: `dt`是為一個`DateTime`的物件, `ts`是為一個`TimeSpan`的物件) 

#### 2-5 民國年(農曆年)的轉換

- `DateTime`及`DateTimeOffset`均有建構式可以用來傳入`Calendar`,以下範例可以用民國年月日來初始化一個日期時間的物件:

  ```c#
  var taiwanCalendar = new System.Globalization.TaiwanCalendar();
  var taiwanDate = new DateTimeOffset(109, 7, 15, 0, 0, 0, 0,  taiwanCalendar, new TimeSpan(8,0,0));
  ```

  - `System.Globalization.TaiwanCalendar()`: 代表民國年, 此外`TaiwanLunisolarCalendar`則為農曆年
  - 所產生的`taiwanDate`實際上是轉換成西元年的內容

- 不要再用`年-1911`的算法(這有漏洞)

  ```c#
  var dt1 = new DateTime(2012, 2, 29);
  var dt2 = dt1.AddYears(-1911); // 看不出有什麼問題嗎?
  ```

  - 這種算法就是,`dt2`得到的日期會是`101/02/28`

- 正確將日期轉回民國年的方法

  ```c#
  var dt1 = new DateTime(2012, 2, 29);
  var taiwanCalendar = new System.Globalization.TaiwanCalendar();
  var taiwanYear = taiwanCalendar.GetYear(dt1);
  ```

  - `taiwanYear`為取得的民國年,同樣可以用`GetMonth(dt1)`, `GetDayOfMonth()`來取得月及日

### 3. 擴充方法

簡單說,就是可以在一個類別裡"加入"方法,這種加入的方式有別於繼承類的結構,可以完全無視原類別裡的設計,就算禁止繼承,也不影響此方法的使用!這是一種很強大的擴充機制,強烈推薦使用!

 以一個範例來說明擴充方法的宣告:

```c#
public static class Extensions
{
    public static long ToUnixTime(this DateTime dt)
    {
        return (long) (dt - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
    }
}
```

(第1行)這個類別必需是靜態類別

(第3行)擴充方法的說明(規則)如下:

- 必需是靜態方法

- 參數寫法`this 類別 參數名`:`this`是固定寫法,會透過`this`將呼叫者的本身物件傳入,`類別`是指`this`的類別,而`dt`是用來替換`this`的變數名稱!

  此方法的意思是,在`DateTime`這個類別裡,"加入"一個方法,這個方法名稱叫:`ToUnixTime()`,此方法沒有參數!而呼叫者本身物件會由`dt`傳入!

如何來使用擴充方法: 

```c#
using SSPushCommon.Core.Helpers; 
...
PushTime = DateTime.Now.ToUnixTime();
```

(第1行)要引入其擴充方法的類別名稱空間

(第3行)呼叫`ToUnixTime()`擴充方法,`DateTime.Now`這個物件會由`dt`傳入

*擴方法注意事項*

- 擴充方法的簽章(指方法`名稱+參數`)與原來類別裡的方法相同時,擴充方法不會被呼叫執行(無法覆蓋原方法)

- 在引入擴充方法時,是以命名空間為單位,當引入一個命名空間時,所有宣告的方法均會被啟用!

- 擴充方法一般是針對無法管理的類別(沒有源始碼)的方法擴充,若是自行設計的類別,應以"繼承"設計方式優先!

- 擴充方法跟繼承的差別之一是,擴充方法不用改動類別名稱(繼承的擴充會產生一個新的類別名稱),因此在一個舊的可管理類別(己被大量使用),避免修改其類別造成的成本上的增加(例如:產生新的bug),也可以考慮使用擴充方法!

- 同上情形,類別的擴充若僅適用在一個專案範圍內(不適用在全體)時,也可考慮用此方式!

### 4. 泛型

#### 4-1. 關於泛型的一些說明

- 泛型的主旨是, 將一個類型的指定,延遲到要用時才指定,而非宣告時
- 提高類別或方法的可重用(Reuse)性
- 限制型別可用的範圍

#### 4-2 定義泛型類別

```c#
public class Generic<T>
{
    private T Field;
}
```

(第1行)在定義一個類別時,同時以`<T>`來宣告一個泛用類別名稱`T`(這是一個類別名,只不過在這時後並不確定它是什麼類別)

(第3行)同時,我們以`T` 類別宣告了一個成員`Field`(這個成員變數的類別叫做`T`,跟第1行的`T`是同一個類別)

```c#
Generic<string> g = new Generic<string>(); 
```

在實體化此物件時,同時指定以`string`來代入泛型`T`(在這時後,`Generic`類別裡的`T`會被`string`取代),而`Field`的型別也才確定為是 `string`.

- 泛型的`T`可以把它當做一個類別的參數,其數量可以多個
- 泛型的`T`的名稱是習慣以`T`來命名(代表Type),實際上可以是其它名稱,例如在dotnet提供的Dictionary類別的定義為:`Dictionary<TKey,TValue>`,這裡的泛型名稱會更有意義
- 各類別的泛型類別,並不是固定用途,其泛型類別的用途要依其類別的設計來使用(要參考其說明文件),但也有一些通用的用法,例如在一個集合的泛型類別,大多數是指這個集合裡的元素類別

#### 4-3 定義泛型方法

```c#
static void Swap<T>(ref T lhs, ref T rhs)
{
    T temp;
    temp = lhs;
    lhs = rhs;
    rhs = temp;
}
```

(第1行)`Swap`方法宣告了一個`T`類型,而`lhs`及`rhs`的類型也會是`T`(第一個`T`是宣告,由呼叫者指定,後面的`T`是使用此類型)

(第3行)這裡的`T`也是使用了宣告的`Swap<T>`

```C#
int a = 1;
int b = 2;
Swap<int>(ref a, ref b);
```

(第3行)在呼叫`Swap` 時指定以`int`型別代入,這時`a`及`b`參數都要以`int` 的型別來傳入

#### 4-4 泛型類別的名稱不要跟泛型方法名稱同名

```c#
class GenericList<T>
{
    // CS0693
    void SampleMethod<T>() { }
}

class GenericList2<T>
{
    //No warning
    void SampleMethod<U>() { }
}
```

(第4行)處會有警告: https://docs.microsoft.com/zh-tw/dotnet/csharp/misc/cs0693

#### 4-5 對泛型使用條件約束

```c#
void SwapIfGreater<T>(ref T lhs, ref T rhs) where T : System.IComparable<T>
{
    T temp;
    if (lhs.CompareTo(rhs) > 0)
    {
        temp = lhs;
        lhs = rhs;
        rhs = temp;
    }
}
```

(第1行) `where T: System.IComparable<T>`: 這是對傳入的`T`類別,必需是`ICompareable`的實作類別(`ICompareable`是一個介面,任何有實作這個介面的類別均可以傳入)

### 5. 集合和資料結構

https://docs.microsoft.com/zh-tw/dotnet/standard/collections/

#### 5-1. 集合的功能特性

- 有列舉集合內所有元素的能力: 所有集合類別均會實作[IEnumerable](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.ienumerable?view=netcore-3.1) (這個介面中定義了`GetEnumerator()`方法,使其可使用在`foreach`的迴圈),或[IEnumerable<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.ienumerable-1?view=netcore-3.1)(可以進一步用在`LINQ`中)
- 將集合元素複製到陣列中: 所有集合都可使用 `CopyTo` 方法複製到陣列。但新陣列中的元素順序，會根據列舉程式傳回元素的順序排列。 產生的陣列一律會是一維陣列，且其下限為零。

#### 5-2. 常用的集合

簡單來說,集合存放的資料分為二種: `單值`及`鍵/值`, `單值`的集合大多繼承自`ICollection`及`IList`,而`鍵/值`的集合大多繼承自`IDictionary`

```mermaid
classDiagram
	IEnumerable <|-- ICollection
	ICollection <|-- IList
	ICollection <|-- IDictionary

```

下表列出常用的集合類別:

| 我想要…                                                     | 泛型集合的選項                                               | 非泛型集合的選項(註1)                                        | 安全執行緒或固定集合的選項                                   |
| :---------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
| 儲存項目為鍵/值對(key-value paired)，以供依據索引鍵快速查詢 | [Dictionary<TKey, TValue>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.dictionary-2) | [Hashtable](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.hashtable)   (註2) | [ConcurrentDictionary](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.concurrent.concurrentdictionary-2)  [ReadOnlyDictionary](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.objectmodel.readonlydictionary-2)  [ImmutableDictionary](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutabledictionary-2) |
| 依索引存取項目                                              | [List<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.list-1) | [Array](https://docs.microsoft.com/zh-tw/dotnet/api/system.array)  [ArrayList](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.arraylist) | [ImmutableList](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablelist-1)  [ImmutableArray](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablearray) |
| 先進先出 (FIFO) 地使用項目                                  | [Queue<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.queue-1) | [Queue](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.queue) | [ConcurrentQueue](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.concurrent.concurrentqueue-1)  [ImmutableQueue](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablequeue-1) |
| 後進先出 (LIFO) 地使用資料                                  | [Stack<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.stack-1) | [Stack](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.stack) | [ConcurrentStack](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.concurrent.concurrentstack-1)  [ImmutableStack](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablestack-1) |
| 循序存取項目                                                | [LinkedList<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.linkedlist-1) | 無                                                           | 無                                                           |
| 當集合中有項目移除或加入時，會收到通知。(註3)               | [ObservableCollection<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.objectmodel.observablecollection-1) | 無                                                           | 無                                                           |
| 排序的集合                                                  | [SortedList<TKey, TValue>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.sortedlist-2) | [SortedList](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.sortedlist) | [ImmutableSortedDictionary](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablesorteddictionary-2)  [ImmutableSortedSet](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablesortedset-1) |
| 數學函式的集合                                              | [HashSet<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.hashset-1)  [SortedSet<T>](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.generic.sortedset-1) | 無                                                           | [ImmutableHashSet](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablehashset-1)  [ImmutableSortedSet](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.immutable.immutablesortedset-1) |

*註1 不建議使用非泛型集合

*註2 (成對的機碼/值之集合，依據機碼的雜湊碼加以組織)。*

*註3 (實作 [INotifyPropertyChanged](https://docs.microsoft.com/zh-tw/dotnet/api/system.componentmodel.inotifypropertychanged) 和 [INotifyCollectionChanged](https://docs.microsoft.com/zh-tw/dotnet/api/system.collections.specialized.inotifycollectionchanged))*

#### 5-4 排序

若要對一個集合中的物件做排序,首先要訂義物件之間的大小關係,這部份要由實作`IComparable<T>`介面裡的`CompareTo()`方法來達成,例如:要對一個`Part`類別的物件做排序:

```C#
public class Part : IComparable<Part>
{
    public string PartName { get; set; }
    public int PartId { get; set; }
    // Default comparer for Part type.
    public int CompareTo(Part comparePart)
    {
          // A null value means that this object is greater.
        if (comparePart == null)
            return 1;
        else
            return this.PartId.CompareTo(comparePart.PartId);
    }
}
```

(第6行)`CompareTo()`必須傳回3種值:

- 大於0: 代表本物件比傳入物件大 (`this `> `comparePart`)
- 0: 代表二值一樣大
- 小於0: 代表本物件比傳入物件小(`this` < `comparePart`)

此外,在這例子中,是利用`int32`類別裡己經實作好的`CompareTo`來回傳,換句話說就是對整數的比較,如果`this.PartId`較大,則回傳一個大於0的值(習慣是回傳1)!

當要執行排序時:(假設`parts`是`Part`類別的集合)

```C#
parts.Sort();
```

即會做出對`Part`裡依照`PartId`的大小來排序!

若在`Part`裡並未實作`CompartTo()`,或是想用不同的方式來排序,`Sort`提供了一個`Comparison<t>`參數,這參數可以傳入一段`Delgate`(或Lambda表達示,其實就是一個Callback function),來實作其比較大小的方法

```C#
parts.Sort((Part x, Part y) => 
    x.PartName == null && y.PartName == null
        ? 0
        : x.PartName == null
            ? -1
            : y.PartName == null
                ? 1
                : x.PartName.CompareTo(y.PartName));
```

這個Lamdba express的參數中,接收二個參數(當前值,下一個值),而表達示裡是用`Part.PartName`來做排大小!

### 6. 錯誤處理(Exception Handling)

#### 6-1. 例外處理

程式的執行流程中,由上層呼叫下層方法,在上層可能會出現一些例外或錯誤的狀況,為了要能處理這種非正常結束的狀況,一般我們會用`try...catch...finally`的語法來處理

*下層方法,用來計算除法*

```C#
public static double Division(double x, double y)
{
    if (y == 0)
        throw new System.DivideByZeroException();
    return x / y;
}
```

*上層呼叫如下*

```C#
double a = 98, b = 0;
double result = 0;
try
{
    result = Division(a, b);
    Console.WriteLine("{0} divided by {1} = {2}", a, b, result);
}
catch (DivideByZeroException e)
{
    Console.WriteLine("Attempted divide by zero.");
}
```

對於例外狀況的說明:

- 例外狀況(例如:`DivideByZeroException`)是最終全都衍生自 `System.Exception` 的型別。
- 在可能擲回例外狀況的陳述式前後使用 `try` 區塊。
- 一旦 `try` 區塊中發生例外狀況之後，控制流程就會跳至第一個相關聯的例外狀況處理常式，此處理常式存在於呼叫堆疊中的任何位置。 在 C# 中，`catch` 關鍵字是用來定義例外狀況處理常式。
- 如果沒有指定例外狀況的例外狀況處理常式存在，程式就會停止執行並出現錯誤訊息。
- (**重要**)除非您可以處理它，否則不要攔截例外狀況，並讓應用程式保持已知的狀態。 如果您攔截 `System.Exception` ，請使用區塊結尾的關鍵字重新擲回 `throw` `catch` 。
- 如果 `catch` 區塊定義了例外狀況變數(這裡是指變數`e`)，您可以使用它來取得所發生例外狀況型別的詳細資訊。
- 例外狀況可以透過程式使用 `throw` 關鍵字來"強制"地產生。
- 例外狀況物件(這裡是指變數`e`)包含錯誤的詳細資訊，例如呼叫堆疊的狀態和錯誤狀態的文字描述。
- 即使擲回例外狀況，`finally` 區塊中的程式碼也會執行。 使用 `finally` 區塊來釋放資源，例如，關閉已在 `try` 區塊中開啟的任何資料流或檔案。

**設計原則(很重要)**

- 不要把例外物件當做參數及回傳值!
- 不要在一個方法裡將非正常結束的訊息,透過回傳值回傳!所有非正常的訊息應在方法裡`擲出`(throw)一個例外!例如:某個方法回傳值大於0代表正常回傳值,而小於0代表發生錯誤!(結果值要單一功能,與例外分離!)

- 不同的例外類型可以為其設計自訂義的例外類別(由`Exception`或其它現有的子類別來繼承)
- 中間層的方法在攔截(catch)到下層方法`擲出`的例外時,不論是否有處理,均必需將例外向上擲回

  - 下層的例外若不處理,可以直接將例外向上擲回,可用`throw;`語句即可完成!
  - 下層例外若經過處理,可以向上擲回一個不同的例外類別,可用`throw new MyException();`來達成!
- 避免利用例外的發生來改變程式處理的流程(不要在`catch`裡寫跟原始流程有關的程式碼),而應該僅對例外的訊息處理及擲回!

### 7. 資料流(Steam)

#### 7-1. 資料流各類別

資料流主要功能是用來串接存取各種不同的來源,例如: file, memory, network, storage...等等

```mermaid
classDiagram
  Steam <|-- FileSteam
  Steam <|-- MemoryStream
  Steam <|-- BufferedStream
  Steam <|-- NetworkStream
  Steam <|-- PipeStream
  Steam <|-- CryptoStream  
```

- `Steam`: `System.IO.Steam`是一個抽象類別,提供主要`read`/`write`/`seek`(並非所有資料流均支援`seek`)功能! (以下類別均繼承自此類別)
- `FileStream`: 操作檔案的資料流(以byte為單位),可以針對任何一種檔案格式
- `MemoryStream`: 操作記憶體的資料流(以byte為單位)
- `BufferStream`: 對其它資料流做介接來提高存取的效能
- `NetworkStream`: 對網路資料(network socket)做存取
- `PipeStream` : 對不同程序的介接
- `CryptoStream`: 對加密資料的介接

#### 7-2. 資料流存取

- `StreamReader`:  讀取資料流的輔助類別,用來讀取資料流裡的`byte`,並提供功能將之轉換成字串(包括編碼)
- `StreamWriter`:  同上,寫入資料流的輔助類別!
- `BinaryReader`: 讀取資料流的輔助類別,讀取為`byte`資料
- `BinaryWriter`: 同上,以`byte`寫入資料流

```mermaid
graph LR
	id1[StreamReader</br>StreamWriter] <== Write String ==> id2[FileStream] == Write bytes ==> id3[Physical File]
	id3 == Read bytes ==> id2 == Read String ==> id1

```

以下是範例

```C#
public class CompBuf
{
    private const string FILE_NAME = "MyFile.txt";

    public static void Main()
    {
        if (!File.Exists(FILE_NAME))
        {
            Console.WriteLine($"{FILE_NAME} does not exist!");
            return;
        }
        FileStream fsIn = new FileStream(FILE_NAME, FileMode.Open,
            FileAccess.Read, FileShare.Read);
        // Create an instance of StreamReader that can read
        // characters from the FileStream.
        using (StreamReader sr = new StreamReader(fsIn))
        {
            string input;
            // While not at the end of the file, read lines from the file.
            while (sr.Peek() > -1)
            {
                input = sr.ReadLine();
                Console.WriteLine(input);
            }
        }
    }
}
```

(第12行)建立一個`FileStream`用來開啟一個檔案

(第16行)透過一個`StreamReader`來串接上的述的`FileStream`

(第20行)`sr.Peek()`可以用來判斷是否後面尚有資料

(第22行)這裡可以直接讀取一行資料

此外,如果去查[文件](https://docs.microsoft.com/zh-tw/dotnet/api/system.io.streamreader.-ctor?view=netcore-3.1#System_IO_StreamReader__ctor_System_String_),可以發現`StreamReader`也可以直接傳入一個檔案名稱,不需要自己先建立一個`FileStream`,然而如果我們去查`StreamReader`的原始碼可以發現,在`StreamReader`的建構式中,會在內部幫我們先建立一個`FileStream`(參見下列原始碼)

```C#
internal StreamReader(String path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize, bool checkHost)
{
    // Don't open a Stream before checking for invalid arguments,
    // or we'll create a FileStream on disk and we won't close it until
    // the finalizer runs, causing problems for applications.
    if (path==null || encoding==null)
        throw new ArgumentNullException((path==null ? "path" : "encoding"));
    if (path.Length==0)
        throw new ArgumentException(Environment.GetResourceString("Argument_EmptyPath"));
    if (bufferSize <= 0)
        throw new ArgumentOutOfRangeException("bufferSize", Environment.GetResourceString("ArgumentOutOfRange_NeedPosNum"));
    Contract.EndContractBlock();

    Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, DefaultFileStreamBufferSize, FileOptions.SequentialScan, Path.GetFileName(path), false, false, checkHost);
    Init(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize, false);
}
```

由此可得知,這個架構實際上並沒有不同!

#### 7-3. `IFileInfo`的`CreateReadStream()`

只要是實作`IFileInfo`的類別,均會有`CreateReadStream()`的方法,這方法可以提供一個`Stream`(一般來說都是唯讀的),再自己透過`Stream`或再串接`StreamReader`來讀取這個檔案,亦或是提供給其它需要`Stream`的處理!

此外,有這種功能的不是只有`IFleInfo`(這裡僅是舉例),另外像是`IIFormFile`(前端提交檔案的接收型別),也有一個`OpenReadStream()`的方法,以下是一段將前端檔案轉傳到Azure Blob的範例片段:

```C#
 public async Task<string[]> UploadToBlob(string containerName, List<IFormFile> files)
 {
     var connectionString = _config.GetSection("AzureSettings")["BlobConnectionString"];
     var container = new BlobContainerClient(connectionString, containerName);
     var filePaths = new List<string>();
     var resultUris = new string[files.Count];
     var counter = 0;
     try
     {
         foreach (var fromFile in files)
         {
             if (fromFile.Length > 0)
             {
                 var groupName = "";
                 if (fromFile.ContentType == "application/vnd.ms-excel")
                     groupName = "excels";
                 else if (fromFile.ContentType.IndexOf("image") > -1)
                     groupName = "images";
                 else
                     groupName = "cers";
                 var blob = container.GetBlobClient(groupName + "/" + fromFile.FileName);
                 await blob.UploadAsync(fromFile.OpenReadStream(), true);
                 BlobProperties properties = await blob.GetPropertiesAsync();
                 resultUris[counter] = blob.Uri.AbsoluteUri;
             }
             counter++;
         }
         return resultUris;
     }
     catch (Exception ex)
     {
         throw;
     }
 }
}
```

(第22處)` blob.UploadAsync()`的第一個參數即為一個檔案的`Stream`!

### 8. 非同步處理(Async/Await)

#### 8-1. 非同步的好處

一般的同步方法在主要執行緒執行時,若此時再去呼叫另一個同步方法,此執行緒會被`block`阻擋而停止為其它同時間需要處理的方法服務!而若呼叫另一個方法是非同步的,則此執行緒會處於`wait`等待的狀態(被呼叫的方法會啟動一個新的執行緒來運作),若有其它需要處理的方法要運行時,會切換去處理另一個方法,進而達到充份利用執行緒的運轉效能!

#### 8-1. 非同步方法的設計

設計一個非同步的方法範例如下

```C#
public async Task<AppDTO[]> List(string userId)
{
    // ...略   
    try
    {
        var ds = await _ssApiClient.SPRetB(_commonHelper.GetServerInfo(), "usp_GetAppMaintain", @params);
        var app = ds["data"]?["table1"]?.ToObject<AppDTO[]>();
        return app;
    }
    catch (Exception ex)
    {
        throw;
    }
}
```

(第1行)`async`用來宣告這是一個非同步方法, 而回傳型別原來是`AppDTO[]`,則要改成`Task<AppDTO[]>`(一個非同步方法的宣告就只有這二個地方要改,相當簡單)

(第6行)呼叫一個非同步方法,並等待`await`直到完成(註1),一個非同步方法裡,必需最少有一個地方是:

- 執行另一個非同步方法並等待`await`

- 發起一個不同的執行緒來執行非同步工作並等待`await`,方式如下:

    ```C#
    await Task.Run(() => { 
        doSyncMethod();
    });
    ```
    
     換句話說,若要將一個同步方法轉成非同步方法,只要將這些方法的執行放入一個`Task.Run()`即可!

> 註1: 若不加上`await`關鍵字,程式運作到這裡時並不會等待完成,而是立刻向下繼續執行!(同時,編輯器會提示一個警告在此)
>
> 註2: `async/await`必需成對出現,若用`async`宣告一個方法,但沒有任何`await`的內容,編輯器會提示一個警告!(這可以允許,但不可以在一個同步方法裡有`await`的內容)

### 9. Lambda表達示

#### 9-1. C#裡的`Func`及`Action`

在C#為了要做到類似`javascript`的`function`([一級函式](https://zh.wikipedia.org/wiki/%E5%A4%B4%E7%AD%89%E5%87%BD%E6%95%B0),first-class function),設計出`Delgate`的機制,而利用這機制,預訂義了`Func`及`Action`二種對於一級函式的宣告(這二者的差別,僅在有無回傳值,`Func`是有回傳值,而`Action`則無),而以下則為宣告一個一級函式的範例:

```C#
Action<string> great = (name) =>
{
    Console.WriteLine(name);
};
```

```C#
Func<string, string> great = (name) => 
{
    return $"hello, {name}!";
}
```

(第1行)`Action`及`Func`泛型`string`是第一個參數`name`的型別,`Func`第二個`string`是回傳值型別,`great`是函式的名稱,可以跟以下的一段`javascrip`對應(等義)

```javascript
// 以下是javascript
var great = function(name) {
    return "hello, " + name + "!";
}
```

呼叫宣告的一級函式如下:

```C#
var myname = great();
```

#### 9-2. Lambda表達示

其實可以把它視為一個匿名的(僅有等號右邊的部份)一級函式,可以有以下幾種寫法:

- 陳述式 (就是有多行的程式區塊): `(input-parameters) => { <sequence-of-statements> }`
- 運算式(僅有單行,且同時為回收傳值):  `(input-parameters) => expression`
- 如果參數只有一個,括號可以省略: `input-parameter => expression`
- 如果沒有參數: `() => expression`

#### 9-3.  Lambda表達示的應用

絕大多數的Lambda表達示都是為了要當參數來傳遞而存在

```C#
customers.Where(c => c.City == "London");
```

(第1行)的`Where`指定要傳入一個`Func<Customer, bool>`的參數,因此可以傳入一個Lambda表達式

#### 9-4. 方法主體用Lambda運算式

一個標準的方法:

```C#
public override string ToString()
{
   return $"{fname} {lname}".Trim();
}
```

如果這方法僅有一行,可以將之改為Lamdba運算式,如下:

```C#
public override string ToString() => $"{fname} {lname}".Trim();
```



### 11. JSON資料處理(針對Newtonsoft.Json)

dotnet core的`System.Text.Json`是用來做Json相關處理的程式庫,是新加入的標準,有別於以往常用的[Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json)(亦稱為JSON.Net),微軟希望將其處理Json的方式做一標準化,但由於其程式庫還很新,很多功能並未完整(甚至有bug),因此在現階段並不推薦用(還是繼續用Json.Net)!

> 註: 這二者的差異可以參考此文件: https://docs.microsoft.com/zh-tw/dotnet/standard/serialization/system-text-json-migrate-from-newtonsoft-how-to

#### 11-1. 基本常用的方法介紹

- 序列化: 將一個物件轉成Json(註:這裡說的Json,泛指Json字串,以下均同)

  ```c#
  Product product = new Product();
  product.Name = "Apple";
  product.ExpiryDate = new DateTime(2008, 12, 28);
  product.Price = 3.99M;
  product.Sizes = new string[] { "Small", "Medium", "Large" };
  string output = JsonConvert.SerializeObject(product);
  return output;
  ```

- 反序列化: 將一個Json轉成物件,在這裡的物件分為二種

  - 直接轉成指定的類別,例如: `Product`,這種方式必需準備一個資料模型類別給`DeserializeObject`的泛型來依其格式來轉換

  ```c#
  Product deserializedProduct = JsonConvert.DeserializeObject<Product>(output);
  ```

  - 反序列化為一個通用的`JObject`,不用特別準備資料模型,任何Json都能轉成`JObject`,但缺點是在操作`JObject`時,必需要知道其Json結構,再對其結構撰寫相應的存取程式碼

  ```c
  JObject product = JObject.Parse(output);
  var productName = product["Name"];
  var productPrice = product["Price"];
  ```

  - 對於`JsonConvert.DeserializeObject()`的使用,若不指定泛型類別,或是指定的類別為`object`,`dynamic`,`JObject`,均會轉成`JObject`(效果等同於`JObject.Parse`),因此,可以將第一種寫法泛用化(當成標準寫法)

  > 註: 建議未來使用在回傳資料庫的大量資料時,均改用`JObject`會較輕量,新版的SSAPI的回傳,也都改成`JObject`做為回傳類型!

  - 序列化的類型是支援使用`DataTable`的,用法同上述,轉出來的Json會長的像下例:

    ```json
    {
      "Table1": [
        {
          "id": 0,
          "item": "item 0"
        },
        {
          "id": 1,
          "item": "item 1"
        }
      ]
    }
    ```
    
  - 反序列化一個`DataSet`,做法也同上述的反序列化,以下是一個範例

    ```c#
    string json = @"
      // 略...(內容同上例)
    ";
    DataSet dataSet = JsonConvert.DeserializeObject<DataSet>(json);
    DataTable dataTable = dataSet.Tables["Table1"];
    Console.WriteLine(dataTable.Rows.Count);
    // 2
    foreach (DataRow row in dataTable.Rows)
    {
        Console.WriteLine(row["id"] + " - " + row["item"]);
    }
    // 0 - item 0
    // 1 - item 1
    ```
  
- 若對序列化及反序列化有更進一步的需求,例如: 要針對欄位值的自定格式轉換, 想將結果存成檔案, null值的特別處理...等等,則要用`JsonSerializer`類別來處理,可以參考官方文件: https://www.newtonsoft.com/json/help/html/SerializingJSON.htm,可以處理的項目列表,官方詳細說明: https://www.newtonsoft.com/json/help/html/SerializationSettings.htm

#### 11-2. `JToken`對一般物件的互轉

- `JToken`要轉成一般物件,它提供了`ToObject<T>()`的方法:

  ```c#
  var app = ds["data"]?["table1"]?.ToObject<AppDTO[]>();
  ```

  在此例中,將`table1`這屬性值轉成`AppDTO`物件

- 一般物件轉成`JToken`,它提供了`FromObject(Object)`的靜態方法:

  ```c#
  JObject jobj = (JObject)JToken.FromObject(appDTO);
  ```

  這裡的`jobj`最好指定型別,若用`JToken`也可以,但可能在使用上會比較不便(例如要使用`JObject`特有方法時,又要再轉一次)

#### 11-3. 對`JToken`及相關類別的操作說明

- `JObject`: 用來存放(對應)一個Json的根(root)為物件型態的資料

  ```C#
  var jobj = new JObject {{"Name", "Mark"}, {"Age", 8 }};
  var info = new JObject {{"Phone", "132****7777"}, {"Gender", "男"}};
  jobj.Add("Info", info);
  jobj.Add(new JProperty("Birthday", new DateTime(1990, 1, 1)));
  ```

  (第1,2行)初始化一個Json物件,並給初始值

  (第3行)利用`Add()`將物件加入另一個"Info"的物件值裡

  (第4行)用`JProperty`來表達"屬性名"及"值"
  最終其Json結果如下:
  
  ```json
  {
      "Name": "Mark",
      "Age": 8,
      "Info": {
          "Phone": "132****7777",
          "Gender": "男"
      },
      "Birthday": "1990-01-01"
  }
  ```

- `JArray`: 用來存放一個Json根(root)為陣列型態的資料

  ```c#
  var jarray = new JArray();
  var mark = new JObject { { "Name", "Mark" }, { "Age", 8 } };
  var jack = new JObject { { "Name", "Jack" }, { "Age", 9 } };
  jarray.Add(mark);
  jarray.Add(jack);
  ```

  (第1行)`JArray`初始化

  (第2,3行)建立2個`JObject`物件

  最終其結果如下:

  ```json
  [{"Name": "Mark, "Age": 8},{"Name": "Jack", "Age": 9}]
  ```

  若要對`JObject`或`JArray`取值:

  ```c#
  jobj["Info"];
  jarray[1];
  ```

  (第1行)取出`jobj`裡的"Info"屬性值(其值是另一個`JObject`,但其宣告型別是`JToken`,因為你不知道取出來的到底是什麼)

  (第2行)取出`jarray`陣列裡的第2項內容(其實際型別是依照放入的物件型別,但其宣告型別依然還是`JToken`

- `JValue`: 對Json值的處理及轉換,若要將一個一般值(int, string, bool, DateTime...等)存放或取出其Json的屬性值中,可以統一用這個類別(僅有單一值時)

  ```c#
  var jobj = new JObject {{"Name", "Mark"}, {"Age", 8}};
  var info = new JValue("my info")
  jobj.Add("Info", info);
  ```

- `JProperty`: 一組"鍵"-"值"對(key-value pair),一般在對一個`JObject`裡的單一屬性及值做處理時,可以用此類別

  ```C#
  JObject o = new JObject
  {
      { "name1", "value1" },
      { "name2", "value2" }
  };
  foreach (JProperty property in o.Properties())
  {
      Console.WriteLine(property.Name + " - " + property.Value);
  }
  // name1 - value1
  // name2 - value2
  ```

  

- `JToken`: 是`JObject`,`JArray`, `JValue`, `JProperty`...等(以上所有的類別,但不僅止於這些)的父類別(此類別是一個抽象類別,不能實體化),用來表達一個通用的Json值,而此值可能是各種情況(`JTokenType`的[列表](https://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_Linq_JTokenType.htm))

## 七、進階篇

### 1. 同介面多個實體的注入

> 本文來自: https://blog.johnwu.cc/article/asp-net-core-3-di-same-interface.html 

通常在使用 ASP.NET Core 依賴注入 (Dependency Injection, DI) 都是一個介面對應一個實作類別。
若有多個類別時做相同的介面時，注入的方式就會有點變化。
本篇將介紹 ASP.NET Core 依賴注入多個相同的介面 (Interface)

*前置準備*

以下介面作為範例：

```C#
public enum WalletType
{
    Alipay,
    CreditCard,
    LinePay
}

public interface IWalletService
{
    WalletType WalletType { get; }
    void Debit(decimal amount);
}

public class AlipayService : IWalletService
{
    public WalletType WalletType { get; } = WalletType.Alipay;
    public void Debit(decimal amount)
    {
        // 從支付寶扣錢
    }
}

public class CreditCardService : IWalletService
{
    public WalletType WalletType { get; } = WalletType.CreditCard;
    public void Debit(decimal amount)
    {
        // 從信用卡扣錢
    }
}

public class LinePayService : IWalletService
{
    public WalletType WalletType { get; } = WalletType.LinePay;
    public void Debit(decimal amount)
    {
        // 從 Line Pay 扣錢
    }
}
```

 *服務註冊*

用相同的介面，註冊不同的實作類別：

```c#
public class Startup
{
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddMvc();

        // 註冊 Services
        services.AddSingleton<IWalletService, AlipayService>();
        services.AddSingleton<IWalletService, CreditCardService>();
        services.AddSingleton<IWalletService, LinePayService>();
    }
}
```

 *Service Injection*

在 Constructor Injection 時用 `IEnumerable<T>` 注入，便可取得相同介面的全部實例，再依照使用情境選擇要用的實例。範例如下：

```C#
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace MyWebsite.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly IEnumerable<IWalletService> _walletServices;

        public ShoppingCartController(IEnumerable<IWalletService> walletServices)
        {
            _walletServices = walletServices;
        }

        public IActionResult Checkout(WalletType walletType, decimal amount)
        {
            var walletService = _walletServices.Single(x => x.WalletType == walletType);
            walletService.Debit(amount);
            return Ok();
        }
    }
}
```

示意圖如下：

![multiple_injection-1](.\assets\multiple_injection-1.png)



> `注意！`
> 通常只建議 **Singleton** 類型的服務這樣使用，因為使用 **Transient** 或 **Scoped** 類型的服務，注入時會 `new` 新的實例，若沒用到的話，就變成不必要的效能耗損。

如果服務註冊用相同的介面，註冊不同的實作類別，Constructor Injection 時不是用 `IEnumerable<T>` 注入，就只會得到最後一個註冊的類別，如上例會得到 `LinePayService` 的實例。

```C#
public ShoppingCartController(IWalletService walletServices)
{
    _walletServices = walletServices;  // 會得到 `LinePayService` 的實例
}
```

示意圖如下：

![multiple_injection-2](.\assets\multiple_injection-2.png)



### 2. 自製ServiceProvider(提供整套服務)

https://blog.johnwu.cc/article/asp-net-core-3-build-service-provider.html

### 3. 中介軟體

https://blog.johnwu.cc/article/asp-net-core-3-middleware.html

### 4. 對Request/Response body動手動腳

https://blog.johnwu.cc/article/asp-net-core-3-read-request-response-body.html

### 5. 程式生命週期

https://blog.johnwu.cc/article/asp-net-core-3-application-lifetime.html

### 6. 跨域請求(Cross-Origin Request)

https://blog.johnwu.cc/article/ironman-day26-asp-net-core-cross-origin-requests.html

### 7. 壓縮Response 資料(Gzip)

https://blog.johnwu.cc/article/ironman-day29-asp-net-core-response-compression.html

### 8. 上傳/下載(大)檔案

https://blog.johnwu.cc/article/ironman-day23-asp-net-core-upload-download-files.html

### 9. NLog

各種logger的比較:https://blog.darkthread.net/blog/high-capacity-dotnet-logging-survey/

https://ithelp.ithome.com.tw/articles/10195968

原生: https://blog.johnwu.cc/article/ironman-day18-asp-net-core-logging.html

### 10. 過濾器(Filter)

https://blog.johnwu.cc/article/ironman-day14-asp-net-core-filters.html

### 11. Cookie & Session

https://blog.johnwu.cc/article/ironman-day11-asp-net-core-cookies-session.html

### 12. 啟用靜態檔案

https://blog.johnwu.cc/article/ironman-day05-asp-net-core-static-files.html

### 13. 資料目錄

App_Data及DataDictionary的設定

### 13. 處理檔案(FileProvider)

### 14. 路由器

https://blog.johnwu.cc/article/ironman-day07-asp-net-core-routing.html

### 15. HttpClient

### 16. 圖片處理

https://devblogs.microsoft.com/dotnet/net-core-image-processing/

### 17. 連接ftp

上傳: https://docs.microsoft.com/zh-tw/dotnet/framework/network-programming/how-to-upload-files-with-ftp
下載: https://docs.microsoft.com/zh-tw/dotnet/framework/network-programming/how-to-download-files-with-ftp
列出目錄: https://docs.microsoft.com/zh-tw/dotnet/framework/network-programming/how-to-list-directory-contents-with-ftp

### 18. Web Service及XML處理

## 八、主題篇

### 1. 反射(Reflection)

### 2. Entity Framework

### 3. SSAPI

### 4. Regular Express正則表達示

請參考: https://docs.microsoft.com/zh-tw/dotnet/standard/base-types/regular-expression-language-quick-reference

## 九、附註篇

### 1.各種字串格式化(來源:https://marcus116.blogspot.com/2018/10/c-stringformat.html)

**標準數值格式**

| 格式 | 說明     | Format | Input     | Output        | 補充                                                         |
| ---- | -------- | ------ | --------- | ------------- | ------------------------------------------------------------ |
| C    | 貨幣     | {0:C}  | 1234.567  | NT$1,234.57   | Currency ：C預設到小數2位…C1取小數一位，C3取小數三位…        |
| D    | 十進位   | {0:D}  | 1234      | 1234          | Decimal： 只支援整數資料型別（integral types），D後面數字表示指定的位數 |
| E    | 科學指數 | {0:E}  | 1234      | 1.234000E+003 | Scientific                                                   |
| F    | 固定     | {0:F}  | 1234.4567 | 1234.46       | Fixed-point                                                  |
| G    | 一般     | {0:G}  | 1234.567  | 1234.57       | General                                                      |
| N    | 數字     | {0:N}  | 120000    | 120,000.00    | Number：每三位數用 "," 隔開                                  |
| P    | 百分比   | {0:P}  | 0.25      | 25.00%        | Percent**：**輸入數值＊100 ; 預設取小數2位，P0可取小數       |
| R    | 來回     | {0:R}  | 0.25      | 0.25          | Round-trip：只支援Double、Single                             |
| X    | 十六進位 | {0:R}  | 123       | 7B            | Hexadecimal：只支援整數資料型別（integral types）            |

**自訂數值格式**

| 格式 | 說明           | Format      | Input | Output  | 補充                                                         |
| ---- | -------------- | ----------- | ----- | ------- | ------------------------------------------------------------ |
| 0    | 零值預留位置   | {0:000.000} | 12.3  | 012.300 | Zero placeholder                                             |
| #    | 數字預留位置   | {0:###.###} | 12.3  | 12.3    | Digit placeholder  #,,：1234567890→1235 #,,,：1234567890→1 #,##0,,：1234567890→1,235 |
| .    | 小數點         | {0:0.0}     | 12.3  | 12.3    | Decimal point                                                |
| ,    | 千位分隔符號   | {0:0,0}     | 1200  | 1,200   | Thousand separator and number scaling                        |
| %    | 百分比預留位置 | {0:0%}      | 0.25  | 25%     | Percentage placeholder                                       |
| e    | 科學標記法     | {0:0e+0}    | 123   | 1e+2    | Scientific notation                                          |
| \    | 跳脫字元       | {0:00\n0}   | 123   | 12 3    | Escape character                                             |

備註：自訂數值格式化：{0:(###) ### – ####} ，1234567890→(123) 456 – 7890，詳細請參考 [自訂數值格式輸出範例](http://msdn.microsoft.com/zh-tw/library/7x5bacwt(v=vs.80).aspx)



**標準DateTime格式**

*下表使用的時間：2012/3/11 下午 01:02*

| 格式 | 說明                | Format | Output                        | 補充                           |
| ---- | ------------------- | ------ | ----------------------------- | ------------------------------ |
| d    | 簡短日期            | {0:d}  | 2012/3/11                     | MM/dd/yyyy                     |
| D    | 完整日期            | {0:D}  | 2012年3月11日                 |                                |
| f    | 完整可排序日期/時間 | {0:f}  | 2012年3月11日 下午 01:02      |                                |
| F    | 完整可排序日期/時間 | {0:F}  | 2012年3月11日 下午 01:02:03   |                                |
| g    | 一般可排序日期/時間 | {0:g}  | 2012/3/11 下午 01:02          |                                |
| G    | 一般可排序日期/時間 | {0:G}  | 2012/3/11 下午 01:02:03       |                                |
| M、m | 月日                | {0:m}  | 3月11日                       |                                |
| o    | 來回日期/時間       | {0:o}  | 2012-03-11T13:02:03.0000000   |                                |
| R、r | RFC1123             | {0:R}  | Sun, 11 Mar 2012 13:02:03 GMT |                                |
| s    | 可排序日期/時間     | {0:s}  | 2012-03-11T13:02:03           |                                |
| t    | 簡短時間            | {0:t}  | 下午 01:02                    | HH:mm                          |
| T    | 完整時間            | {0:T}  | 下午 01:02:03                 | HH:mm:ss                       |
| u    | 通用可排序日期/時間 | {0:u}  | 2012-03-11 13:02:03Z          | yyyy'-'MM'-'dd HH':'mm':'ss'Z' |
| U    | 通用可排序日期/時間 | {0:U}  | 2012年3月11日 上午 05:02:03   |                                |
| Y、y | 年月                | {0:y}  | 2012年3月                     |                                |

> 註：輸出日期格式顯示可以依據〈控制台〉→ 〈地區語言選項〉做修改，詳細請參考：[標準DateTime格式輸出範例](http://msdn.microsoft.com/zh-tw/library/hc4ky857(v=vs.80).aspx)、[String.Format yyyy/MM/dd? 誤會大了](http://blog.darkthread.net/post-2009-04-01-date-formate-slash.aspx) by 黑暗大

**自訂DateTime格式**

測試時間：2012/3/11 下午 02:21

| 格式   | 說明                     | Format         | Output     | 補充                                             |
| ------ | ------------------------ | -------------- | ---------- | ------------------------------------------------ |
| dd     | 月份日期                 | {0:dd}         | 11         |                                                  |
| ddd    | 星期幾的縮寫             | {0:ddd}        | 星期日     | Sun                                              |
| dddd   | 星期幾的完整名稱         | {0:dddd}       | 星期日     | Sunday                                           |
| f, ff… | 秒數                     | {0:fff}        | 364        |                                                  |
| gg,…   | 時期或時代               | {0:gg}         | 西元       |                                                  |
| hh     | 小時（12 小時制）        | {0:hh}         | 02         |                                                  |
| HH     | 小時（24 小時制）        | {0:HH}         | 14         |                                                  |
| mm     | 分鐘                     | {0:mm}         | 21         |                                                  |
| MM     | 月份                     | {0:MM}         | 03         |                                                  |
| MMM    | 月份的縮寫名稱           | {0:MMM}        | 三月       | Mar                                              |
| MMMM   | 月份的完整名稱           | {0:MMMM}       | 三月       | March                                            |
| ss     | 秒數                     | {0:ss}         | 49         |                                                  |
| tt     | A.M./P.M                 | {0:tt}         | 下午       |                                                  |
| yy     | 兩個位數的數字來表示年份 | {0:yy}         | 12         |                                                  |
| yyy    | 三個位數的數字來表示年份 | {0:yyy}        | 2012       |                                                  |
| yyyy   | 四個位數的數字來表示年份 | {0:yyyy}       | 2012       |                                                  |
| zz     | 時差（小時）             | {0:zz}         | +08        | 系統時區與格林威治標準時間 (GMT) 時差            |
| zzz    | 時差（小時＆分鐘）       | {0:zzz}        | +08:00     | 系統時區與格林威治標準時間 (GMT) 時差 (帶正負號) |
| :      | 時間分隔符號             | {0:hh:mm:ss}   | 02:29:06   |                                                  |
| /      | 日期分隔符號             | {0:yyyy/MM/dd} | 2012/03/11 |                                                  |

詳細請參考 [自訂DateTime格式字串](http://msdn.microsoft.com/zh-tw/library/8kb3ddd4(v=vs.80).aspx)



## 十、擴展篇

這份文件雖然只寫到這裡,但不代表就結束了,希望大家在開發的過程中,有什麼心得(可能是你查了半天的問題,或是一個簡單的觀念突然想通了,或是一段你很得意的程式片段,再或者是你的心情抒發...等等)不論大小,都希望你能把它紀錄下來!希望這份文件能變成大家後遇到問題時,第一個想要來找的寶庫!



