using System.Threading.Tasks;

namespace tr.core.Services.Interfaces
{
    public interface IFileService
    {
        Task<string> ReadTextAsync(string path);
        
        
    }
}