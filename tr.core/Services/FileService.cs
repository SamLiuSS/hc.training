using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.FileProviders;
using tr.core.Services.Interfaces;

namespace tr.core.Services
{
    public class FileService:IFileService
    {
        private readonly IFileProvider _fileProvider;

        public FileService(IFileProvider fileProvider)
        {
            _fileProvider = fileProvider;
        }

        public async Task<string> ReadTextAsync(string path)
        {
            byte[] buffer;
            using (var stream = _fileProvider.GetFileInfo(path).CreateReadStream())
            {
                buffer = new byte[stream.Length];
                await stream.ReadAsync(buffer, 0, buffer.Length);
            }

            return Encoding.Default.GetString(buffer);
        }

    }
}