using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using tr.core.Services;
using tr.core.Services.Interfaces;

namespace tr.core
{
    public class Startup
    {
        private readonly IHostEnvironment _env;

        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration, IHostEnvironment env)
        {
            _env = env;

            Configuration = configuration;
        }

        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            var applicationRoot = _env.ContentRootPath;
            //services.AddSingleton<IFileProvider>(new PhysicalFileProvider(applicationRoot));
            services.AddSingleton<IFileProvider>(_env.ContentRootFileProvider);
            services.AddSingleton<IFileService,FileService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
