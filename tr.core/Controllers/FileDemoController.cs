﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tr.core.Models;
using tr.core.Services.Interfaces;

namespace tr.core.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class FileDemoController : ControllerBase
    {
        private readonly ILogger<FileDemoController> _logger;
        private readonly IFileService _fileService;

        public FileDemoController(ILogger<FileDemoController> logger, IFileService fileService)
        {
            _fileService = fileService;
            _logger = logger;
        }
        [HttpGet]
        public async Task<FileModel> Get()
        {
            return new FileModel
            {
                fileString = await _fileService.ReadTextAsync("data.txt")
            };
        }
        [HttpGet("{fileName}/{times}")]
        public async Task<FileModel> GetByName(string fileName, int times)
        {
            var returnString = "";
            for (var i=0; i < times; i++)
            {
                returnString += await _fileService.ReadTextAsync(fileName);

            }
            return new FileModel
            {
                fileString = returnString
            };
        }
        [HttpGet]
        public async Task<IActionResult> GetBad()
        {
            var file = new FileModel
            {
                fileString = await _fileService.ReadTextAsync("data.txt")
            };
            return BadRequest(file);
        }
    }

}
