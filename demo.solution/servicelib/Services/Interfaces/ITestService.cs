namespace servicelib.Services
{
    public interface ITestService
    {
        string getTestName(string name);
    }
}