﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using servicelib.Services;

namespace webapp1.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Test1Controller : ControllerBase
    {
        private readonly ILogger<Test1Controller> _logger;
        private readonly ITestService _testService;

        public Test1Controller(ILogger<Test1Controller> logger, ITestService testService)
        {
            _logger = logger;
            _testService = testService;
        }

        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            var result = _testService.getTestName(name);
            return Ok(result);
        }
    }
}
