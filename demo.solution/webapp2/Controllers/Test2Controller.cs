﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using servicelib.Services;

namespace webapp2.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class Test2Controller : ControllerBase
    {
      
        private readonly ILogger<Test2Controller> _logger;
        private readonly ITest2Service _testService;

        public Test2Controller(ILogger<Test2Controller> logger, ITest2Service testService)
        {
            _logger = logger;
            _testService = testService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var result = _testService.getTestName("app2");
            return Ok(result);
        }
    }
}
