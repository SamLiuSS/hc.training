﻿using System;
using Newtonsoft.Json.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var jarray = new JArray();
            
            var jobj = new JObject {{"Name", "Mark"}, {"Age", 8 }};
            var info = new JObject {{"Phone", "132****7777"}, {"Gender", "男"}};
            jobj.Add("Info", info);
            jobj.Add("jv", new JValue("help"));
            jobj.Add("empty",jarray);
            jobj.Add(new JProperty("Birthday", new DateTime(1990, 1, 1)));
            
            JToken v = jobj["empty"];
            JToken jt = new JArray();
            Console.WriteLine("Hello World!");
        }
    }
}